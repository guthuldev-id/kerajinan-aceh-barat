<?php 
	
	define('RONAL',TRUE);
	require_once 'setting.php';
	require_once fungsi;
	
	if (!cek_sessi_user()) {
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ?act=Home'>";
	}

	$id_pesan = $_GET['id'];
	$sql = "SELECT id_pesan, keterangan, id_jarak FROM pesan WHERE id_pesan='$id_pesan' ORDER BY id_pesan DESC LIMIT 0,1";
	list($id_pesan,$ket,$id_jarak) = mysql_fetch_row(query($sql));
	
	$tarif = tarif_jarak($id_jarak);

	$sql = "SELECT u.nama, p.tujuan, p.kota, p.propinsi, p.kd_pos, p.telepon, u.email ".
		   "FROM user AS u, pesan AS p ".
		   "WHERE p.id_user=u.id_user AND p.id_pesan='$id_pesan'";
	$row = mysql_fetch_row(query($sql));

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
	<title>Cetak Nota Pemesanan</title>
</head>

<style>

body {
	font-family:Arial, Helvetica, sans-serif;
	padding:0;
	font-size:12px;
	margin:0px auto auto auto;
	color:#000000;
}

a {
	color:#b04c26;
	text-decoration:none;
}

a:hover {
	color:#000;
	cursor:pointer;
}

table {
	margin:0 2px 0 7px;
	border-collapse: collapse;
	border-spacing:0;
	border:1px solid #e8e7e1;
	background:none;
}

table td, th{
	padding:5px;
	border:1px solid #e8e7e1;
} 

table th{
	padding:5px;
	background:#f7f6f0;
}

.left_content {
	width:600px;
	float:left;
	margin:10px 0 10px 10px;
	padding:10px;
	background:#fff;
}

.left_box {
	width:595px;
	border:1px solid #e8e7e1;
	min-height:150px;
	margin:0 2px 10px 2px;
}

.left_box h3 {
	width:585px;
	border-bottom:1px solid #e8e7e1;
	background:#f7f6f0;
	padding:5px;
	margin:0 0 5px 0;
}

.left_bg {
	width:573px;
	border:1px solid #e8e7e1;
	margin:5px;
	padding:5px;
}

.row {
	width:550px;
	clear:both;
	padding:5px 0 5px 0;
}

label.nota3 {
	width:120px;
	float:left;
	font-size:12px;
	text-align:right;
	padding:0 5px 0 0;
	color: #333333;
}

.close {
	padding:0 0 0 20px;
	background:transparent url(./images/ico_logout.png) no-repeat left;
	font-weight:bold;
}

.print {
	padding:0 0 0 20px;
	background:transparent url(./images/ico_print.png) no-repeat left;
	font-weight:bold;
}

</style>

<script type="text/javascript">

	function print_page() {
		if (typeof(window.print) != 'undefined') {
			window.print();
		}
	}

</script>

<body>

	<div class="left_content">
		<div class="left_box">											
			<h3>Nota Belanja Anda</h3>
			
			<table width="98%">
				<tr> 
					<th colspan="6" align="center"><strong>Kode Pemesanan Anda : <?=$id_pesan?></strong></th>
				</tr>
				<tr>
					<th width="20px">No</th>
					<th>Nama Produk</th>
					<th>Harga</th>
					<th widht="50px">Berat</th>
					<th width="30px">Qty</th>
					<th>Total</th>
				</tr>
			
			<?php	
				
				$sql = "SELECT p.produk, d.jumlah_barang, d.harga_satuan, (p.berat * d.jumlah_barang) AS sberat, ".
					   "(d.jumlah_barang * d.harga_satuan) AS stotal ".
					   "FROM produk AS p, pesan_detail AS d ".
					   "WHERE d.id_produk=p.id_produk AND d.id_pesan='$id_pesan'";
				$isi = query($sql);
				$no=0;
				while (list($produk,$jum,$harga,$sberat,$stotal) = mysql_fetch_row($isi))	{	
					$no++;		
					$total += $stotal;
					$berat += $sberat;

					echo "<tr $no>";
					echo "<td align='center'>".$no."</td>";
					echo "<td align='left'>".ucwords($produk)."</td>";
					echo "<td align='right'>".format_uang($harga)."</td>";
					echo "<td align='center'>".$sberat."Kg</td>";
					echo "<td align='center'>".$jum."</td>";
					echo "<td align='right'>".format_uang($stotal)."</td>";
					echo "</tr>";
				} 

				if ($no>0) {
					$sberat = ceil($berat);
					$starif = $tarif*$sberat;
					$skabeh = $total+$starif;
					echo "<tr><td colspan='6' align='right'>
						<strong>Total Belanja Anda : </strong>".format_uang($total)."</td></tr>";
					echo "<tr><td colspan='6' align='right'>
						<strong>Ongkos Kirim Per Kg : </strong>".format_uang($tarif)."</td></tr>";
					echo "<tr><td colspan='6' align='right'>
						<strong>Total Ongkos Kirim : </strong>".format_uang($starif)."</td></tr>";
					echo "<tr><td colspan='6' align='right'>
						<strong>Total Keseluruhan : </strong>".format_uang($skabeh)."</td></tr>";
				} else {
					echo "<tr><td colspan='6'><marquee behavior='alternate'>Keranjang Belanja Masih Kosong</marquee></td></tr>";
				}

			?>
				</table>
				<div class="left_bg"><strong>Terbilang : </strong><?=terbilang($skabeh,3)?></div>
				<div class="left_bg">
				
				<div class="row">
					<label class="nota3"><strong>Atas Nama :</strong></label>
					<?=ucwords($row[0])?>
				</div>
				<div class="row">
					<label class="nota3"><strong>Alamat Tujuan :</strong></label>
					<?=ucwords($row[1])?>
				</div>
				<div class="row">
					<label class="nota3"><strong>Kota Tujuan :</strong></label>
					<?=ucwords($row[2])?>
				</div>
				<div class="row">
					<label class="nota3"><strong>Propinsi Tujuan :</strong></label>
					<?=ucwords($row[3])?>
				</div>
				<div class="row">
					<label class="nota3"><strong>Kode Pos Tujuan :</strong></label>
					<?=ucwords($row[4])?>
				</div>
				<div class="row">
					<label class="nota3"><strong>Telepon Pemesan :</strong></label>
					<?=ucwords($row[5])?>
				</div>
				<div class="row">
					<label class="nota3"><strong>Email Pemesan :</strong></label>
					<?=$row[6]?>
				</div>
			</div>
			
			<div class="left_bg"><strong>Keterangan Pemesanan :</strong><br /><p><?=$ket?></p></div>
			<div class="left_bg"><?php include 'page/user/penting.php' ?></div>
			<div class="left_bg">
				<a href="" class="print" onclick="javascript:print_page()">Cetak</a>&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="" class="close" onclick="window.close()">Tutup</a>
			</div>
			
		</div>
	</div>

</body>
</html>