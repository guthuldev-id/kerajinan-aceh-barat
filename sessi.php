<?php
	
	session_start();

	define('RONAL',TRUE);
	
	require_once 'setting.php';
	require_once fungsi;

	if (!cek_sessi_user()) {
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ?act=Home'>";
	}

	if (!isset($_SESSION['ses_total'])) $_SESSION['ses_total']=0; 

	if ($_REQUEST['sts']==='clear') {
		for ($i=0; $i<$_SESSION['ses_total']; $i++) {
			unset($_SESSION['ses_kode'][$i]);
			unset($_SESSION['ses_jum'][$i]);
		}
		$_SESSION['ses_total'] = 0;			
	
	} else if ($_REQUEST['sts']==='delete') {
		for ($i=0; $i<$_SESSION['ses_total']; $i++) {
			if ($_REQUEST['kode']===$_SESSION['ses_kode'][$i]) {
				unset($_SESSION['ses_kode'][$i]);
				unset($_SESSION['ses_jum'][$i]);
				break;
			}
		}
	
	} else if (isset($_POST['kode'])) {
		if (($_POST['jum']=='') || ($_POST['jum'] < 1) || ($_POST['jum'] > 10)) {
			echo "<script>alert('Jumlah Barang Pesanan Harus Diantara 1 s/d 10 Barang.!');window.history.back();</script>";
			exit;
		}
	
		if ($_REQUEST['sts']==='edit') {
			for ($i=0; $i<$_SESSION['ses_total']; $i++) {
				if ($_POST['kode']===$_SESSION['ses_kode'][$i]) {
					$_SESSION['ses_jum'][$i] = $_POST['jum'];
					break;
				}
			}		
		
		} else {
			$ada = false;
			for ($i=0; $i<$_SESSION['ses_total']; $i++) {
				if ($_POST['kode']===$_SESSION['ses_kode'][$i]) {
					$_SESSION['ses_jum'][$i] += $_POST['jum'];
					$ada = true;
					break;
				}
			}
			
			if (!$ada) {
				$_SESSION['ses_kode'][$_SESSION['ses_total']]= $_POST['kode'];
				$_SESSION['ses_jum'][$_SESSION['ses_total']] = $_POST['jum'];			
				$_SESSION['ses_total']++;
			}
		}
	}

	header("location:index.php?page=pesan.keranjang");

?>