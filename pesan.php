<?php
	
	session_start();
	
include_once 'includes/koneksi.php';
include_once 'includes/fungsi.php';

	if (!cek_sessi_user()) {
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ?page=home'>";
	}

	$ses_total = $_SESSION['ses_total'];
	$ses_kode  = $_SESSION['ses_kode'];
	$ses_jum   = $_SESSION['ses_jum'];
	$tgl	   = date('Ymd');
	$kd_user   = kode_konsumen($_SESSION['USER_RONAL']);
	$kd_pesan  = kode_pesan();
	$alamat    = valid_form($_POST['alamat']);
	$kota	   = valid_form($_POST['kota']);
	$propinsi  = valid_form($_POST['propinsi']);
	$kd_pos	   = valid_form($_POST['kd_pos']);
	$telp	   = valid_form($_POST['telepon']);
	$kd_jarak  = valid_form($_POST['jarak']);
	$ket	   = valid_form($_POST['keterangan']);
	$tarif	   = tarif_jarak($kd_jarak);
	$berat	   = ceil($_POST['berat']);
	$total	   = $_POST['total'];
	$tarifnya  = $tarif; //$berat*$tarif;
	$stotal    = $total+$tarifnya;

	if (empty($alamat) || empty($kota) || empty($propinsi) || empty($telp) || empty($kd_jarak)) {
		pesan_error("Data masih ada yang kosong");
		exit;
	}

	$sql = "INSERT INTO pesan (id_pesan,id_user,id_jarak,tgl,keterangan,ttl_bayar,tujuan,kota,propinsi,kd_pos,telepon) ".
		   "VALUES ('$kd_pesan','$kd_user','$kd_jarak','$tgl','$ket','$stotal','$alamat','$kota','$propinsi','$kd_pos','$telp')";
	query($sql);

	for ($i=0; $i < $ses_total; $i++) {
		if ($ses_kode[$i]=='') continue;
		$kode = $ses_kode[$i];
		$jum  = $ses_jum[$i];
		$harga = fetch_row("SELECT harga FROM produk WHERE id_produk='$kode'");
		$sql = "INSERT INTO pesan_detail (id_pesan,id_produk,harga_satuan,jumlah_barang) ".
			   "VALUES ('$kd_pesan','$kode','$harga','$jum')";
		query($sql);
	}

	# hapus keranjang belanja 	
	for ($i=0; $i<$_SESSION['ses_total']; $i++) {
		unset($_SESSION['ses_kode'][$i]);
		unset($_SESSION['ses_jum'][$i]);
	}
	$_SESSION['ses_total'] = 0;			

	header("Location: index.php?page=pesan.proses");

?>