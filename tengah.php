<?php 
if (isset($_GET['page'])) {
	switch($_GET['page']) {

	case 'home' :
    include "page/home.php"; 
	break;

	case 'jenis' :
    include "page/jenis.php"; 
	break;

	case 'login' :
    include "page/login.php"; 
	break;

	case 'daftar' :
    include "page/daftar.php"; 
	break;

	case 'profil' :
    include "page/profil.php"; 
	break;

	case 'cari' :
    include "page/cari.php"; 
	break;

	case 'profil.edit' :
    include "page/user/profil_edit.php"; 
	break;

	case 'profil.view' :
    include "page/user/profil_view.php"; 
	break;

	case 'produk' :
    include "page/produk.php"; 
	break;

	case 'berita' :
    include "page/berita.php"; 
	break;
	
	case 'faq' :
    include "page/faq.php"; 
	break;

	case 'kontak' :
    include "page/bukutamu.php"; 
	break;

	case 'detail' :
    include "page/detail.php"; 
	break;

	case 'berita' :
    include "page/berita.php"; 
	break;

	case 'pesan.keranjang' :
    include "page/pesan_keranjang.php"; 
	break;

	case 'pesan.checkout' :
    include "page/pesan_checkout.php"; 
	break;

	case 'pesan.proses' :
    include "page/pesan_proses.php"; 
	break;	
	
	case 'pesan.cek' :
    include "page/user/cek_pesan.php"; 
	break;

	case 'konf.bayar' :
    include "page/user/konf_bayar.php"; 
	break;
}
}else{
include "page/home.php";
}
?>