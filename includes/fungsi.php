<?php


	function query($query) {		
		$result = @mysql_query($query) 
		or die("<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ./'>");
		return $result;
	}

	function fetch_row($query) {
		$tmp = query($query);
		list($result) = mysql_fetch_row($tmp);
		return $result;
	}

	function num_rows($query) {
		$tmp = query($query);
		$jum = mysql_num_rows($tmp);
		return $jum;
	}
	
	function valid_form($string){
		return htmlentities(addslashes($string));
	}

	function cek_sessi_admin() {
		if ($_SESSION['ADMIN_RONAL'])
			return true;
		else
			return false;
	}
	
	function cek_sessi_user() {
		if ($_SESSION['USER_RONAL'])
			return true;
		else
			return false;
	}

	function cek_user_admin($user) {
		$br = fetch_row("SELECT user FROM admin WHERE user='$user'");
		if ($br!='') 
			return true;
		else
			return false;
	}

	function cek_user($user) {
		$br = fetch_row("SELECT user FROM user WHERE user='$user'");
		if ($br!='') 
			return true;
		else
			return false;
	}

	function cek_pass_admin($user, $pass) {
		$br = fetch_row("SELECT user FROM admin WHERE user='$user' AND pass=md5('$pass')");
		if ($br!='') 
			return true;
		else
			return false;
	}

	function cek_pass($user, $pass) {
		$br = fetch_row("SELECT user FROM user WHERE user='$user' AND pass=md5('$pass')");
		if ($br!='') 
			return true;
		else
			return false;
	}

	function cek_email($email, $check_domain = false) {
        if ($check_domain){ }
        if (ereg('^[-!#$%&\'*+\\./0-9=?A-Z^_`a-z{|}~]+'.'@'.
                 '[-!#$%&\'*+\\/0-9=?A-Z^_`a-z{|}~]+\.'.
                 '[-!#$%&\'*+\\./0-9=?A-Z^_`a-z{|}~]+$', $email))
        {
            if ($check_domain && function_exists('checkdnsrr')) {
                list (, $domain)  = explode('@', $email);
                if (checkdnsrr($domain, 'MX') || checkdnsrr($domain, 'A')) {
                    return true;
                }
                return false;
            }
            return true;
        }
        return false;
    }

	function cek_sex($sex) {
		if ($sex=='p') return "Pria";
		else if ($sex=='w') return "Wanita";
		else return "Ga Boleh";
	}

	function cek_cowo($sex) {
		if ($sex=='p') return "checked";
		else return "";
	}

	function cek_cewe($sex) {
		if ($sex=='w') return "checked";
		else return "";
	}

	function cek_ada($sts) {
		if ($sts=='ada') return "checked";
		else return "";
	}

	function cek_tidak($sts) {
		if ($sts=='tidak') return "checked";
		else return "";
	}

	function no_baris($j="") {
		if ($j=="") $jum = 5;
		else $jum = $j;
		if (isset($_GET["page"]) && ($_GET["page"]>1))
			return $jum*($_GET["page"]-1);
		else
			return 0;
	}
	
	function pager_isi($sql,$limit) {
		$isi = query($sql);
		$jum = mysql_num_rows($isi);
		if (($jum % $limit) == 0) $jumpage = (int)($jum/$limit);
		else $jumpage = (int)(($jum/$limit)+1);
		
		if (isset($_GET["page"])) $page = $_GET["page"];
		else $page = 1;

		if ($page>$jumpage) $page = $jumpage;

		while ($rows = mysql_fetch_array($isi)) {
			$arrdata[] = $rows;
		}

		$end = ($page*$limit)-1;
		$start = $end-($limit-1);

		if ($end > $jum) $end = $jum-1;

		for ($i=$start; $i<=$end; $i++) $arr[] = $arrdata[$i];
		return $arr;
	}

	function pager($sql,$limit,$path) {
		$isi = query($sql);
		$jum = mysql_num_rows($isi);
		if (($jum % $limit) == 0) $jumpage = (int)($jum/$limit);
		else $jumpage = ((int)$jum/$limit)+1;
		if (isset($_GET["page"])) $page = $_GET["page"];
		else $page = 1;
		if ($page>$jumpage) $page = $jumpage;

		echo "<div class='pagination'>Page : ";
		for ($n=1; $n<=$jumpage; $n++) {
			$b = $page + 1;
			if ($n != $page) {
				echo "<a href='$path&page=$n'>$n</a>";
			} else {
				echo "<a disabled class='active'>$n</a>";
			}
		}
		echo '</div>';
	}

	function paging($sql,$limit) {
		require_once(clas.'Pager/Pager.php');
		$isi = query($sql);
		$myData = array();
		while ($row = mysql_fetch_row($isi)) {
			$myData[] = $row;
		}		
		@mysql_free_result($isi);
		$params = array(
			'itemData' => $myData,
			'perPage' => $limit,
			'delta' => 5,
			'append' => true,
			'separator' => '',
			'clearIfVoid' => false,
			'urlVar' => 'entrant',
			'useSessions' => false,
			'closeSession' => false,
			'mode'  => 'Jumping',
		);

		$pager = & Pager::factory($params);
		return $pager;
	}

	function pesan_error($s='') {
		echo "<script type=\"text/javascript\">alert(\"Maaf, $s..!!\");window.history.back();</script>";
	}

	function pesan_submit($url='') {
		if ($url==='') $url = './';
		echo "<script type=\"text/javascript\">alert(\"Data Berhasil Disimpan..!!\");</script>";
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = $url'>";
	}

	function pesan_delete($url='') {
		if ($url==='') $url = './';
		echo "<script type=\"text/javascript\">alert(\"Data Berhasil Dihapus..!!\");</script>";
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = $url'>";
	}

	function selected($t1,$t2) {
		if (trim($t1)==trim($t2)) return "selected";
		else return "";
	}
	
	function combo_propinsi($prop='',$all=false) {
		$temp = file('propinsi.txt');
		echo "<option value='0' selected>- Pilih Propinsi -</option>";
		foreach ($temp as $key) {
			echo "<option value='$key' ".selected($key,$prop).">".$key."</option>";
		}
	}

	function combo_jenis($kdJnis="") {
		echo "<option value='' selected>- Pilih Jenis Produk -</option>";
		$query = query("SELECT * FROM jenis ORDER BY id_jenis");
		while ($row=mysql_fetch_row($query)) {
			if ($kdJnis=="") echo "<option value='$row[0]'> ".ucwords($row[1])." </option>";
			else echo "<option value='$row[0]'".selected($row[0],$kdJnis)."> ".ucwords($row[1])." </option>";
		}
	}

	function combo_produk($sts="") {
		echo "<option value='' selected>- Pilih Status Produk -</option>";
		if ($sts=="") {
			echo "<option value='1'> 1 : Pembelian Harus Langsung Di Toko </option>";
			echo "<option value='2'> 2 : Pembelian Dapat Dilakukan Secara Online</option>";
		} else {
			$no = "1"; 
			$ya = "2";
			echo "<option value='1'".selected($no,$sts)."> 1 : Pembelian Harus Langsung Ke Toko </option>";
			echo "<option value='2'".selected($ya,$sts)."> 2 : Pembelian Dapat Dilakukan Secara Online</option>";
		}
	}

	function combo_jarak($kdJrk="") {
		echo "<option value='' selected>- Pilih Jarak Tujuan Kirim -</option>";
		$query = query("SELECT * FROM jarak ORDER BY id_jarak");
		while ($row=mysql_fetch_row($query)) {
			if ($kdJrk=="") echo "<option value='$row[0]'> ".ucwords($row[1])." - ".format_uang($row[2])." </option>";
			else echo "<option value='$row[0]'".selected($row[0],$kdJrk).">".ucwords($row[1])." - ".format_uang($row[2])."</option>";
		}
	}

	function combo_cari() {
		echo "<option value='' selected>Kategori Cari</option>";
		echo "<option value='1'>Berita</option>";
		echo "<option value='2'>FAQ</option>";
	}

	function format_uang($duit,$space=false) {
		$rp = ($space)?"Rp. ":"Rp.";
		return $rp.number_format($duit,0,",",".").",- ";
	}

	function convert_bulan($bln) {
		switch($bln) {
			case "01": $bulan="Januari"; break;
			case "02": $bulan="Februari"; break;
			case "03": $bulan="Maret"; break;
			case "04": $bulan="April"; break;
			case "05": $bulan="Mei"; break;
			case "06": $bulan="Juni"; break;
			case "07": $bulan="Juli"; break;
			case "08": $bulan="Agustus"; break;
			case "09": $bulan="September"; break;
			case "10": $bulan="Oktober"; break;
			case "11": $bulan="Nopember"; break;
			case "12": $bulan="Desember"; break;
			default  : $bulan="Tidak Boleh";
		}		
		return $bulan;
	}
	
	function convert_tanggal($time) {
		list($thn,$bln,$tgl)=explode('-',$time);
		$tmp = $tgl." ".convert_bulan($bln)." ".$thn;
		return $tmp;
	}

	function convert_tgl($tanggal) {
		list($thn,$bln,$tgl)=explode('-',$tanggal);
		return $tgl.'-'.$bln.'-'.$thn;
	}

	function kadaluarsa($tgl) {
		return fetch_row('SELECT DATE_ADD("'.$tgl.'", INTERVAL 7 DAY)');
	}

	function huruf($x) {
		$x = abs($x);
		$angka = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($x <12)						$temp = " ". $angka[$x];
		else if ($x <20)				$temp = huruf($x - 10). " belas";
		else if ($x <100)				$temp = huruf($x/10)." puluh". huruf($x % 10);
		else if ($x <200) 				$temp = " seratus" . huruf($x - 100);
		else if ($x <1000)				$temp = huruf($x/100) . " ratus" . huruf($x % 100);
		else if ($x <2000)				$temp = " seribu" . huruf($x - 1000);
		else if ($x <1000000)			$temp = huruf($x/1000) . " ribu" . huruf($x % 1000);
		else if ($x <1000000000) 		$temp = huruf($x/1000000) . " juta" . huruf($x % 1000000);
		else if ($x <1000000000000)		$temp = huruf($x/1000000000) . " milyar" . huruf(fmod($x,1000000000));
		else if ($x <1000000000000000)	$temp = huruf($x/1000000000000) . " trilyun" . huruf(fmod($x,1000000000000));
		else							$temp = "wedew";
		return $temp;
	}

	function terbilang($x, $style=4) {
		if($x<0) {
			$hasil = "minus ". trim(huruf($x))." rupiah";
		} else {
			$hasil = trim(huruf($x))." rupiah";
		}      
		switch ($style) {
			case 1:  $hasil = strtoupper($hasil); break;
			case 2:  $hasil = strtolower($hasil); break;
			case 3:  $hasil = ucwords($hasil); break;
			default: $hasil = ucfirst($hasil); break;
		}      
		return $hasil;
	}

	function nama_konsumen($kode) {
		return fetch_row("SELECT nama FROM user WHERE id_user='$kode'");
	}
	
	function kode_konsumen($user) {
		return fetch_row("SELECT id_user FROM user WHERE user='$user'");		
	}
	
	function kode_pesan() {
		$kode_temp=fetch_row("SELECT id_pesan FROM pesan ORDER BY id_pesan DESC LIMIT 0,1");
		if ($kode_temp == '') 
			$kode = "P00001";
		else {
			$jum = substr($kode_temp,1,5);
			$jum++;
			if ($jum <= 9) $kode = "P0000".$jum;
			elseif ($jum <= 99) $kode = "P000".$jum;
			elseif ($jum <= 999) $kode = "P00".$jum;
			elseif ($jum <= 9999) $kode = "P0".$jum;
			elseif ($jum <= 99999) $kode = "P".$jum;
			else die ("Kode pesan lewat batas");
		}
		return $kode;
	}

	function tarif_jarak($kode) {
		return fetch_row("SELECT ongkos FROM jarak WHERE id_jarak='$kode'");
	}

	function total_bayar($kode) {
		return fetch_row("SELECT ttl_bayar FROM pesan WHERE id_pesan='$kode'");
	}

	function cek_bayar() {
		$id_user = kode_konsumen($_SESSION['USER_RONAL']);
		$temp = fetch_row("SELECT sts_bayar FROM pesan WHERE sts_bayar='belum' AND id_user='$id_user'");
		if ($temp=='') return true;
		else return false;
	}

	function combo_tgl($tgl='') {
		if ($tgl=="") $now=date("d");
		else $now=$tgl;
		$jum_hr = date("t");		
		for ($i=1; $i<=$jum_hr; $i++) {
			echo "<option value='$i' ".selected($i,$now).">$i</option>";
		}
	}

	function combo_bln($bln='') {
		if ($bln=="") $now=date("m");
		else $now=$bln;
		$jum_bl = 12;
		for ($i=1; $i<=$jum_bl; $i++) {
			echo "<option value='$i' ".selected($i,$now).">".convert_bulan($i)."</option>";
		}
	}

	function combo_thn($thn='') {
		if ($thn=="") {
			$now=date("Y")-1;
			$thn=date("Y");
		} else $now=$thn;
		$jum_th = 3;		
		for ($i=1; $i<=$jum_th; $i++) {
			echo "<option value='$now' ".selected($thn,$now).">".$now."</option>";			
			$now++;
		}
	}

	function combo_tgl_lap() {
		echo "<option value='' selected>Tgl</option>";		
		for ($i=1,$jum_hr=31; $i<=$jum_hr; $i++) {
			echo "<option value='$i'>$i</option>";
		}
	}

	function combo_bln_lap() {		
		echo "<option value='' selected>Bln</option>";
		for ($i=1,$jum_bl=12; $i<=$jum_bl; $i++) {
			echo "<option value='$i'>".substr(convert_bulan($i),0,3)."</option>";
		}
	}

	function combo_thn_lap() {
		$now=date("Y")-2;
		echo "<option value='' selected>Thn</option>";
		for ($i=1,$jum_th = 5; $i<=$jum_th; $i++) {
			echo "<option value='$now'>$now</option>";
			$now++;
		}
	}

?>