  <?php
  error_reporting(0);
  ob_start();	
  session_start();
include_once 'includes/koneksi.php';
include_once 'includes/fungsi.php';
include_once 'includes/class_paging.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en">
<meta name="robots" content="index, follow">
<meta name="description" content="Ichi Shop">
<meta name="keywords" content="Ichi Shop">
<meta http-equiv="Copyright" content="Ichi Shop">
<meta name="author" content="Vichi">
<meta http-equiv="imagetoolbar" content="no">
<meta name="language" content="Indonesia">
<meta name="revisit-after" content="7">
<meta name="webcrawlers" content="all">
<meta name="rating" content="general">
<meta name="spiders" content="all">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Kerajinan Aceh Barat</title>
<link rel="stylesheet" type="text/css" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css" />
<script type="text/javascript" src="validasi.js"></script>
<script type="text/javascript" src="js/jquery-1.4.1.min.js"></script>
<script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.8.6.min.js"></script>
<script type="text/javascript" src="js/jquery_slide.js"></script>
<script type="text/javascript">

$(window).load(function() {
    $('#slider').nivoSlider({
        effect:'sliceUp', //Specify sets like: 'fold,fade,sliceDown, sliceDownLeft, sliceUp, sliceUpLeft, sliceUpDown, sliceUpDownLeft' 
        slices:1,
        animSpeed:400,
        pauseTime:8000,
        startSlide:0, //Set starting Slide (0 index)
        directionNav:false, //Next & Prev
        directionNavHide:false, //Only show on hover
        controlNav:true, //1,2,3...
        controlNavThumbs:false, //Use thumbnails for Control Nav
        controlNavThumbsFromRel:false, //Use image rel for thumbs
        controlNavThumbsSearch: '.jpg', //Replace this with...
        controlNavThumbsReplace: '_thumb.jpg', //...this in thumb Image src
        keyboardNav:true, //Use left & right arrows
        pauseOnHover:true, //Stop animation while hovering
        manualAdvance:false, //Force manual transitions
        captionOpacity:1, //Universal caption opacity
        beforeChange: function(){},
        afterChange: function(){},
        slideshowEnd: function(){} //Triggers after all slides have been shown
    });
});
</script>

<script type="text/javascript" src="ext/jquery/bxGallery/jquery.bxGallery.1.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="ext/jquery/fancybox/jquery.fancybox-1.3.4.css" />
<script type="text/javascript" src="ext/jquery/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="ext/960gs/960_24_col.css" />
<link rel="stylesheet" type="text/css" href="stylesheet.css" />
</head>
<body>

<div id="bodyWrapper" class="container_24">
<div class="header">

                  <div  id="headerShortcuts">
               <!-- <span class="tdbLink"><a id="tdb1" href="">Advanced Search</a></span>
				<script type="text/javascript">$("#tdb1").button({icons:{primary:"ui-icon-triangle-1-e"}}).addClass("ui-priority-secondary").parent().removeClass("tdbLink");</script>
				<span class="tdbLink"><a id="tdb2" href="">Create an Account</a></span>
				<script type="text/javascript">$("#tdb2").button({icons:{primary:"ui-icon-triangle-1-e"}}).addClass("ui-priority-secondary").parent().removeClass("tdbLink");</script>
				<span class="tdbLink"><a id="tdb3" href="">Shipping & Returns</a></span>
				<script type="text/javascript">$("#tdb3").button({icons:{primary:"ui-icon-triangle-1-e"}}).addClass("ui-priority-secondary").parent().removeClass("tdbLink");</script>                                                 
                                                  <a class="log_in" href="" >Login</a>-->
                                                
                                                                 
                    </div>

        <div class="clear "></div>  
        <div class="top_line"></div>  

            <div id="storeLogo">
              <a href="./"><h1 style="color: white;font-size: 30px;"><strong>Kerajinan Aceh Barat</strong></h1></a>          
            </div>
            <div class="main_menu ">
            
            <span class="tdbLink"><a id="tdb4" href="./">Home page</a></span>
			<script type="text/javascript">$("#tdb4").button({icons:{primary:"ui-icon-triangle-1-e"}}).addClass("ui-priority-secondary").parent().removeClass("tdbLink");</script>
			<span class="tdbLink"><a id="tdb5" href="?page=profil">Profil</a></span>
			<script type="text/javascript">$("#tdb5").button({icons:{primary:"ui-icon-triangle-1-e"}}).addClass("ui-priority-secondary").parent().removeClass("tdbLink");</script>
			<span class="tdbLink"><a id="tdb6" href="?page=produk">Produk</a></span>			
			<script type="text/javascript">$("#tdb6").button({icons:{primary:"ui-icon-triangle-1-e"}}).addClass("ui-priority-secondary").parent().removeClass("tdbLink");</script>
			<span class="tdbLink"><a id="tdb7" href="?page=berita">Berita</a></span>	
			<script type="text/javascript">$("#tdb7").button({icons:{primary:"ui-icon-triangle-1-e"}}).addClass("ui-priority-secondary").parent().removeClass("tdbLink");</script>
			<span class="tdbLink"><a id="tdb8" href="?page=faq">FAQ</a></span>
			<script type="text/javascript">$("#tdb8").button({icons:{primary:"ui-icon-triangle-1-e"}}).addClass("ui-priority-secondary").parent().removeClass("tdbLink");</script>
			<span class="tdbLink"><a id="tdb9" href="?page=kontak">Kontak</a></span>
			<script type="text/javascript">$("#tdb9").button({icons:{primary:"ui-icon-triangle-1-e"}}).addClass("ui-priority-secondary").parent().removeClass("tdbLink");</script>				
                        </div>
              
            <script   type="text/javascript">
                    $('.main_menu a').each(function() {
                  if((window.location.pathname == '/' && $(this).attr('href').indexOf('index.php') > -1) || window.location.href.indexOf($(this).attr('href')) > -1)  $(this).addClass('active');
            })
            </script>
            <div class="clear"></div> 
         
               <div class="show">
                    <div class="left">
                          <div style="margin-left:0px;">
                              <a href="?page=login"><img src="images/new/satu.jpg" alt="Left Banner 1" title=" Left Banner 1 " width="236" height="246" /></a>                            
							  </div>                         
                    </div>
                                <div  id="slider" class="nivoSlider ">
                                
                                    <img  src="images/new/kasab.jpg" alt="">
                                    
                                </div>
                                
             <!--<a href="index.php?cPath=23" class="start_shopping">Start Shopping</a>  -->
                 
             </div>             
            <div class="clear"></div>

      <div class="clear"></div>    
     
</div>            <div class="breadcrumb"> &nbsp;&nbsp;<a href="" class="headerNavigation">Top</a> &raquo; <a href="" class="headerNavigation">Catalog</a></div><img src="images/bg_content.png" alt="" /><br />
<div class="bg_content">
<div id="bodyContent" class="grid_20
           push_4">

<?php include"tengah.php"; ?>

	   </div>

<!--        B a n n e r s   Bottom    //-->

</div> <!-- bodyContent //-->


<div id="columnLeft" class="grid_4 pull_20">
  <!--<div class="box_curr" >  
  <div class="title_curr">Currencies</div>  <div class="content_curr">    
  <form name="currencies" action="" method="get">    
  <select name="currency" onchange="this.form.submit();" style="width: 100%">
  <option value="USD" selected="selected">U.S. Dollar</option>
  <option value="EUR">Euro</option>
  </select>
  <input type="hidden" name="osCsid" value="acd47360488c201ade02478bcf744b65" />
  </form>  </div></div>
<div class="box_lang" >  
<div class="content_lang"> 
<a href=""><img src="images/icon1.gif" alt="English" title=" English " width="24" height="15" /></a>  
<a href=""><img src="images/icon2.gif" alt="Deutsch" title=" Deutsch " width="24" height="15" /></a>  
<a href=""><img src="images/icon3.gif" alt="Espa??????l" title=" Espa??????l " width="24" height="15" /></a> 
</div></div>-->
<div class="box_shopping_cart"  ><div class="title_shoping_cart ">
<?php
	if (empty($_SESSION['ses_total'])){?>
			<a  href="">Shopping Cart</a><div class=" content_shoping_cart">
			<a href="">0 items</a></div></div><div class="clear"></div></div>
			<?php }else{
	$ses_totala = $_SESSION['ses_total'];
	$ses_kodea  = $_SESSION['ses_kode'];
	$ses_juma   = $_SESSION['ses_jum'];
	$noa=0;
	for ($a=0; $a < $ses_totala; $a++) {
	if ($ses_kodea[$a]=='') continue;
	$sqla = "SELECT id_produk, produk, harga, berat FROM produk WHERE id_produk='$ses_kodea[$a]'";
	$isia = query($sqla);
	list ($kode,$nama,$harga,$berat) = mysql_fetch_row($isia);
	$noa++;
	//$sberata = $berat*$ses_juma[$a];
	$stotala = $harga*$ses_juma[$a];
	$totala += $stotala;
	}
			?>
			<a  href="?page=pesan.keranjang">Shopping Cart</a><div class=" content_shoping_cart">
			<a href=""><?=$noa?> items</a></div></div><div class="clear"></div></div>
			<?php }?>
<div class="box_cat"> <h1>Log in</h1>
  <div class="content_cat">
  <div class="last_cat">
<?php
include "includes/login.php";
	  ?>
  </div></div></div>
<div class="box_cat"> <h1>Kategori</h1>
  <div class="content_cat">
  <div class="last_cat">
 <!-- <a class="links  level-0" href="">Animal T-Shirts</a><br />
  <a class="links  level-0" href="">Band T-Shirts</a><br />
  <a class="links  level-0" href="">Beer T-Shirts</a><br />
  <a class="links  level-0" href="">Cheap T-Shirts</a><br />
  <a class="links  level-0" href="">Comic Book T-Shirts</a><br />
  <a class="links  level-0" href="">Military T-Shirts<img src="images/img_level.gif" class="level_img"></a><br />
  <a class="links  level-0" href="">Movie T-Shirts</a><br />
  <a class="links  level-0" href="">Sneakers</a><br />
  <a class="links  level-0" href="">Tie Dye T-Shirts</a><br />-->
  	<?php $sql=mysql_query("select * from jenis");
	while($rs=mysql_fetch_array($sql)){
	echo"<a class='links  level-0' href='?page=jenis&id=$rs[id_jenis]'>$rs[jenis]</a><br />";
	} ?>
  </div></div></div>
  <div class="box_cat"> <h1>Best Seller</h1>
  <div class="content_cat">
  <div class="last_cat">
<?php
$sql = mysql_query("select sum(a.jumlah_barang) as jml,a.id_produk,b.produk,gambar from pesan_detail a, produk b where a.id_produk=b.id_produk group by a.id_produk,jumlah_barang,b.produk order by a.jumlah_barang desc limit 1");
while($bs=mysql_fetch_array($sql)){
echo"<a href='?page=detail&id=$bs[id_produk]' alt='$bs[produk]' title='$bs[produk]'><img src='$bs[gambar]' width='125'></a>";
}
	  ?>
  </div></div></div> 
  <div class="box_cat"> <h1>Support Online</h1>
  <div class="content_cat">
  <div class="last_cat">
<img src="images/online.gif"/>
  </div></div></div>
<div class="box_search"  >  <div class="content_search">    <form name="quick_find" action="?page=cari" method="post">    <input type="text" name="kata" style=" margin-left:15px; margin-top:20px; border:0; background:#dbd8d8; float:left; padding-left:5px;  width:160px; height:18px; font-size:12px; line-height:18px; color:#383838;"  />&nbsp;<div class="img_search"><input type="image" src="images/search.jpg" alt="Quick Find" title=" Quick Find " /> </div>   </form>  </div></div>
<!--<div class="man">  <h1>Manufacturers</h1>  <div class="content_man"><span><form name="manufacturers" action="" method="get"><select name="manufacturers_id" onchange="this.form.submit();" size="1" style=" border:0;  padding:3px 3px 3px 0; width: 200px; "><option value="" selected="selected">Please Select</option><option value="2">Band T-Shirts</option><option value="3">Funny T-Shirts</option><option value="10">Music T-Shirts</option><option value="7">Retro T-Shirts</option></select><input type="hidden" name="osCsid" value="acd47360488c201ade02478bcf744b65" /></form></span></div></div>
<div class="box_best_sellers">  <h1>Bestsellers</h1>  <div class="content_best_sellers"><ol ><li><a href="">Music T-Shirts</a></li><li><a href="">Funny T-Shirts</a></li><li><a href="">Lorem ipsum dolor sit amet,</a></li><li><a href="">Military T-Shirts</a></li><li><a href="">Movie T-Shirts</a></li><li><a href="">Occupation T-Shirts</a></li><li><a href="">Offensive T-Shirts</a></li></ol></div></div>
<div class="box_specials">  <h1>
						Specials
					  </h1>  <div class="content_specials" >
								
									<div class="bg_specials">
								<a class="content_specials_img" href=""><img src="images/product_9.jpg" alt="Tie Dye T-Shirts" title=" Tie Dye T-Shirts " width="158" height="166" /></a><br />
								
								<a class="special_product" href="">
								Tie Dye T-Shirts
								</a>
								<br />
								<del>$190.00</del>
								
								<span class="productSpecialPrice">$19.00</span>
					<div class="clear"></div>
									</div>
						</div>
						
					</div>-->



  <!--        B a n n e r s     Left Column   //-->

  
</div>


<div class="clear"></div>
   
</div> <!-- bg_content //-->
</div> <!-- bodyWrapper //-->

<div class="footer">
<div class="footer_bg">


   <!-- <img src="images/img_footer.jpg" border="0" class="img_footer "/>
             
    
               <!--<a href="" >Home Page</a>
               <a href="" >Advanced Search</a>
               <a href="" >What? New</a>
               <a href="" >My Account</a>
               <a href="" >Reviews</a>
               <a href="" >Create an Account</a>
               <a href="" >Shipping & Returns</a>
               <a href="" >Contact Us</a>
               
                      <br />-->
              <p class="copy">Kerajinan Aceh Barat &copy; 2017</p>
        <div class="clear"></div> 
  
</div>
</div>

<script type="text/javascript">
$('.productListTable tr:nth-child(even)').addClass('alt');
</script>

</body>
</html>
