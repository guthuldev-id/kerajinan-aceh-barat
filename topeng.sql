-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2018 at 05:11 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `topeng`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `user` varchar(32) NOT NULL DEFAULT '',
  `pass` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`user`, `pass`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `bayar`
--

CREATE TABLE `bayar` (
  `id_bayar` int(10) UNSIGNED NOT NULL,
  `id_pesan` varchar(6) NOT NULL,
  `tgl_bayar` date DEFAULT NULL,
  `no_bukti` varchar(16) NOT NULL,
  `jum_bayar` int(10) UNSIGNED DEFAULT NULL,
  `sts_kirim` enum('sudah','belum') NOT NULL DEFAULT 'belum'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bayar`
--

INSERT INTO `bayar` (`id_bayar`, `id_pesan`, `tgl_bayar`, `no_bukti`, `jum_bayar`, `sts_kirim`) VALUES
(1, 'P00002', '2012-05-01', 'n001', 102000, 'sudah'),
(4, 'P00004', '2012-05-15', '14646045', 62000, 'sudah'),
(3, 'P00003', '2012-05-12', 'fgfg', 77000, 'sudah'),
(5, 'P00005', '2012-05-15', '542875272174', 172000, 'sudah'),
(6, 'P00005', '2012-05-15', '542875272174', 172000, 'sudah'),
(7, 'P00005', '2012-05-15', '542875272174', 172000, 'belum'),
(8, 'P00008', '2018-01-09', '345345', 145000, 'sudah'),
(9, 'P00002', '2018-01-09', 'n001', 102000, 'sudah');

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id` tinyint(5) UNSIGNED NOT NULL,
  `berita` text NOT NULL,
  `tgl` date DEFAULT NULL,
  `jam` time DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id`, `berita`, `tgl`, `jam`) VALUES
(1, '<h1><span style="font-weight: bold;">Kini Siapapun Bisa Jadi Fashion Stylist</span></h1>\r\n<br />Dunia fashion memang tak ada matinya. Sama seperti halnya teknologi, \r\nsetiap detiknya kita dapat merasakan adanya aliran denyut berdetak dari \r\ndunia fashion dan sudah tentu banyak juga dari kita yang tak jarang \r\nmenjadi korban tren fashion masa kini. Tentunya Anda tak ingin bukan \r\ndisebut pribadi yang ketinggalan jaman dengan gaya begitu membosankan \r\ndari waktu ke waktu selalu statis. Nah, kalaupun Anda adalah pribadi \r\nyang melek akan fashion tentu Anda akan terus mengikuti perkembangannya,\r\n entah dari internet, TV ataupun majalah. Begitu banyak desainer luar \r\ndan dalam negeri yang menginspirasi akan gaya dari pakaian yang Anda \r\nkenakan saat ini. Tapi apakah pernah terbesit di otak Anda, untuk \r\nmenjadi desainer sendiri, seorang fashion stylist yang dapat menciptakan\r\n tren dengan tampilan gaya sendiri?\r\n<p style="text-align: justify;">Kedengarannya menarik, tapi sayang hal \r\ntersebut tidaklah semua orang mendapatkan kesempatan tersebut. Keinginan\r\n terpendam tersebut mungkin hanyalah sesuatu yang akhirnya Anda kubur \r\ndalam benak Anda jauh tak tersentuh. Tapi nyatanya, sebuah media online \r\nmemberikan sarana buat Anda untuk dapat membuat fashion sendiri, nah \r\nAnda ingin mencobanya? Siapa tahu Anda memiliki bakat terpendam seorang \r\nstylist atau paling tidak ini akan menjadi inspirasi untuk gaya baru \r\npenampilan Anda. Tertarik untuk memulainya?</p>\r\n<p style="text-align: justify;">Siapapun kini dapat menjadi seorang \r\nfashion stylist, dimana Anda dapat memadu padankan rancangan baju untuk \r\nseorang model, tentu hal tersebut bisa dibilang susah-susah mudah. Tapi \r\ntentu semua orang memiliki selera tersendiri yang membuat dia mencintai \r\nsuatu gaya tersendiri. Ada pribadi yang senang jadi pengikut saja, tapi \r\nada juga pribadi yang ingin menciptakan gayanya sendiri. Akhirnya tiba \r\nke hadapan kita : Looklet.com yang menjadi suatu inspirasi bagi Anda \r\nuntuk membuktikan selera Anda berpakaian dan berpenampilan. Disuguhi \r\ndengan ratusan baju atasan, celana, rok, sepatu, kalung, tas dan \r\nlainnya, Anda dapat memilah-milah atau malah menciptakan gaya unik ala \r\nAnda sendiri. Tentu hal ini dapat secara gratis Anda gunakan. Sebelumnya\r\n Anda harus registrasi terlebih dahulu untuk dapat menyimpan hasil karya\r\n Anda. Nah, selanjutnya marilah memilih mana yang cocok untuk model \r\npilihan Anda. Setelah selesai, Anda juga dapat menyimpannya menjadi file\r\n gambar yang dapat Anda simpan, mungkin suatu hari Anda ingin \r\nmewujudkannya menjadi nyata menjadi suatu padanan yang menarik hati. \r\nNah, tunggu apalagi? Silahkan coba dan buktikan, dan jangan kaget kalau \r\nsekali Anda mencobanya akan ketagihan lagi dan lagi.</p>', '2012-05-15', '06:26:38'),
(2, '<h1><span style="font-weight: bold;">Menjadi Seorang Fashionista</span></h1><h4>Dewasa ini fashion sangat mempengaruhi budaya hidup manusia, khususnya kawula muda. Demi menunjukan eksist jiwanya tak sedikit dari mereka nyang merogoh kocek hingga jutaan rupiah demi memenuhi kebutuhan kepuasan batin dan kebutuhan penampilan mereka.</h4><h4>Dewasa ini fashion sangat mempengaruhi budaya hidup manusia, \r\nkhususnya kawula muda. Demi menunjukan eksist jiwanya tak sedikit dari \r\nmereka nyang merogoh kocek hingga jutaan rupiah demi memenuhi kebutuhan \r\nkepuasan batin dan kebutuhan penampilan mereka.</h4><h4>Dewasa ini fashion sangat mempengaruhi budaya hidup manusia, \r\nkhususnya kawula muda. Demi menunjukan eksist jiwanya tak sedikit dari \r\nmereka nyang merogoh kocek hingga jutaan rupiah demi memenuhi kebutuhan \r\nkepuasan batin dan kebutuhan penampilan mereka.</h4><h4 style="text-align: left;"><span style="font-size: 13pt; line-height: 150%;">\r\n    <br /></span></h4><!--[if gte mso 9]><xml>\r\n <w:WordDocument>\r\n  <w:View>Normal</w:View>\r\n  <w:Zoom>0</w:Zoom>\r\n  <w:TrackMoves/>\r\n  <w:TrackFormatting/>\r\n  <w:PunctuationKerning/>\r\n  <w:ValidateAgainstSchemas/>\r\n  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>\r\n  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>\r\n  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>\r\n  <w:DoNotPromoteQF/>\r\n  <w:LidThemeOther>EN-US</w:LidThemeOther>\r\n  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>\r\n  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>\r\n  <w:Compatibility>\r\n   <w:BreakWrappedTables/>\r\n   <w:SnapToGridInCell/>\r\n   <w:WrapTextWithPunct/>\r\n   <w:UseAsianBreakRules/>\r\n   <w:DontGrowAutofit/>\r\n   <w:SplitPgBreakAndParaMark/>\r\n   <w:DontVertAlignCellWithSp/>\r\n   <w:DontBreakConstrainedForcedTables/>\r\n   <w:DontVertAlignInTxbx/>\r\n   <w:Word11KerningPairs/>\r\n   <w:CachedColBalance/>\r\n  </w:Compatibility>\r\n  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>\r\n  <m:mathPr>\r\n   <m:mathFont m:val="Cambria Math"/>\r\n   <m:brkBin m:val="before"/>\r\n   <m:brkBinSub m:val="--"/>\r\n   <m:smallFrac m:val="off"/>\r\n   <m:dispDef/>\r\n   <m:lMargin m:val="0"/>\r\n   <m:rMargin m:val="0"/>\r\n   <m:defJc m:val="centerGroup"/>\r\n   <m:wrapIndent m:val="1440"/>\r\n   <m:intLim m:val="subSup"/>\r\n   <m:naryLim m:val="undOvr"/>\r\n  </m:mathPr></w:WordDocument>\r\n</xml><![endif]--><!--[if gte mso 9]><xml>\r\n <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true"\r\n  DefSemiHidden="true" DefQFormat="false" DefPriority="99"\r\n  LatentStyleCount="267">\r\n  <w:LsdException Locked="false" Priority="0" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Normal"/>\r\n  <w:LsdException Locked="false" Priority="9" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="heading 1"/>\r\n  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 2"/>\r\n  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>\r\n  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 4"/>\r\n  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 5"/>\r\n  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 6"/>\r\n  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 7"/>\r\n  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 8"/>\r\n  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 9"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="toc 1"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="toc 2"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="toc 3"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="toc 4"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="toc 5"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="toc 6"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="toc 7"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="toc 8"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="toc 9"/>\r\n  <w:LsdException Locked="false" Priority="35" QFormat="true" Name="caption"/>\r\n  <w:LsdException Locked="false" Priority="10" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Title"/>\r\n  <w:LsdException Locked="false" Priority="1" Name="Default Paragraph Font"/>\r\n  <w:LsdException Locked="false" Priority="11" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Subtitle"/>\r\n  <w:LsdException Locked="false" Priority="22" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Strong"/>\r\n  <w:LsdException Locked="false" Priority="20" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Emphasis"/>\r\n  <w:LsdException Locked="false" Priority="59" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Table Grid"/>\r\n  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Placeholder Text"/>\r\n  <w:LsdException Locked="false" Priority="1" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="No Spacing"/>\r\n  <w:LsdException Locked="false" Priority="60" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Shading"/>\r\n  <w:LsdException Locked="false" Priority="61" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light List"/>\r\n  <w:LsdException Locked="false" Priority="62" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Grid"/>\r\n  <w:LsdException Locked="false" Priority="63" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 1"/>\r\n  <w:LsdException Locked="false" Priority="64" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 2"/>\r\n  <w:LsdException Locked="false" Priority="65" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 1"/>\r\n  <w:LsdException Locked="false" Priority="66" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 2"/>\r\n  <w:LsdException Locked="false" Priority="67" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 1"/>\r\n  <w:LsdException Locked="false" Priority="68" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 2"/>\r\n  <w:LsdException Locked="false" Priority="69" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 3"/>\r\n  <w:LsdException Locked="false" Priority="70" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Dark List"/>\r\n  <w:LsdException Locked="false" Priority="71" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Shading"/>\r\n  <w:LsdException Locked="false" Priority="72" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful List"/>\r\n  <w:LsdException Locked="false" Priority="73" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Grid"/>\r\n  <w:LsdException Locked="false" Priority="60" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Shading Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="61" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light List Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="62" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Grid Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="63" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="64" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="65" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 1 Accent 1"/>\r\n  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Revision"/>\r\n  <w:LsdException Locked="false" Priority="34" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="List Paragraph"/>\r\n  <w:LsdException Locked="false" Priority="29" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Quote"/>\r\n  <w:LsdException Locked="false" Priority="30" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Intense Quote"/>\r\n  <w:LsdException Locked="false" Priority="66" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 2 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="67" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="68" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="69" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="70" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Dark List Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="71" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Shading Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="72" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful List Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="73" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Grid Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="60" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Shading Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="61" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light List Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="62" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Grid Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="63" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="64" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="65" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 1 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="66" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 2 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="67" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="68" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="69" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="70" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Dark List Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="71" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Shading Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="72" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful List Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="73" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Grid Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="60" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Shading Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="61" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light List Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="62" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Grid Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="63" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="64" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="65" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 1 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="66" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 2 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="67" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="68" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="69" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="70" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Dark List Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="71" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Shading Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="72" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful List Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="73" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Grid Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="60" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Shading Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="61" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light List Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="62" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Grid Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="63" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="64" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="65" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 1 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="66" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 2 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="67" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="68" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="69" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="70" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Dark List Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="71" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Shading Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="72" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful List Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="73" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Grid Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="60" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Shading Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="61" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light List Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="62" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Grid Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="63" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="64" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="65" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 1 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="66" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 2 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="67" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="68" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="69" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="70" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Dark List Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="71" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Shading Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="72" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful List Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="73" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Grid Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="60" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Shading Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="61" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light List Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="62" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Grid Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="63" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="64" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="65" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 1 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="66" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 2 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="67" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="68" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="69" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="70" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Dark List Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="71" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Shading Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="72" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful List Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="73" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Grid Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="19" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Subtle Emphasis"/>\r\n  <w:LsdException Locked="false" Priority="21" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Intense Emphasis"/>\r\n  <w:LsdException Locked="false" Priority="31" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Subtle Reference"/>\r\n  <w:LsdException Locked="false" Priority="32" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Intense Reference"/>\r\n  <w:LsdException Locked="false" Priority="33" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Book Title"/>\r\n  <w:LsdException Locked="false" Priority="37" Name="Bibliography"/>\r\n  <w:LsdException Locked="false" Priority="39" QFormat="true" Name="TOC Heading"/>\r\n </w:LatentStyles>\r\n</xml><![endif]--><!--[if gte mso 10]>\r\n<style>\r\n /* Style Definitions */\r\n table.MsoNormalTable\r\n	{mso-style-name:"Table Normal";\r\n	mso-tstyle-rowband-size:0;\r\n	mso-tstyle-colband-size:0;\r\n	mso-style-noshow:yes;\r\n	mso-style-priority:99;\r\n	mso-style-qformat:yes;\r\n	mso-style-parent:"";\r\n	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;\r\n	mso-para-margin:0cm;\r\n	mso-para-margin-bottom:.0001pt;\r\n	mso-pagination:widow-orphan;\r\n	font-size:11.0pt;\r\n	font-family:"Calibri","sans-serif";\r\n	mso-ascii-font-family:Calibri;\r\n	mso-ascii-theme-font:minor-latin;\r\n	mso-fareast-font-family:"Times New Roman";\r\n	mso-fareast-theme-font:minor-fareast;\r\n	mso-hansi-font-family:Calibri;\r\n	mso-hansi-theme-font:minor-latin;\r\n	mso-bidi-font-family:"Times New Roman";\r\n	mso-bidi-theme-font:minor-bidi;}\r\n</style>\r\n<![endif]--><h4>\r\n  <br /></h4>', '2012-05-15', '06:26:00');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` tinyint(5) UNSIGNED NOT NULL,
  `tanya` text NOT NULL,
  `jawab` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `tanya`, `jawab`) VALUES
(1, 'Bagaimana Cara Memesan Ichi Shop?', '- Untuk User yang belum mempunyai Akun, Silahkan Daftar  dan mengisi Form data member.\r\n- Setelah Mempunyai Akun, Silahkan Login dengan User dan Password\r\n- Mulai berbelanja dengan Memilih barang yang telah disediakan, isi jjumlah Barang yang akan dipesan kemudian klik Pesan.\r\nLet\\\\\\''s Enjoy Shoping'),
(2, 'Bagaimana Cara Melakukan Pembayaran Transaksi?', 'Setelah anda selesai berbelanja dan menginputkan di mana kota tempat tinggal.\r\nSilahkan Melakukan pembayaran melalui Bank ke no.rek:\r\n-Bca\r\na.n      Siti Khodiyah\r\nNo.rek: 0842563263\r\natau\r\n-BRI\r\na.n     Andi saputra\r\nNo.rek: 147857941688\r\nSetelah Anda melakukan pembayaran segera Konfirmasi Ke Link Konfirmasi Pembayaran kemudian Barang yang anda pesan segera dikirim..\r\nSelamat Berbelanja ^^');

-- --------------------------------------------------------

--
-- Table structure for table `halaman`
--

CREATE TABLE `halaman` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `halaman` varchar(64) NOT NULL,
  `konten` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `halaman`
--

INSERT INTO `halaman` (`id`, `halaman`, `konten`) VALUES
(1, 'Profil', '&nbsp;\r\nfgffgfgfg\r\n<br />');

-- --------------------------------------------------------

--
-- Table structure for table `jarak`
--

CREATE TABLE `jarak` (
  `id_jarak` int(5) UNSIGNED NOT NULL,
  `tujuan` varchar(64) NOT NULL,
  `ongkos` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jarak`
--

INSERT INTO `jarak` (`id_jarak`, `tujuan`, `ongkos`) VALUES
(3, 'Yogyakarta', 2000),
(4, 'Bandung', 5000),
(5, 'Padang', 15000),
(6, 'Jakarta', 10000),
(7, 'Surabaya', 8000),
(8, 'YRiau', 2000),
(9, 'Papua', 5000),
(10, 'riau', 15000),
(11, 'Sulawesi Tenggara', 10000),
(12, 'Surabaya', 8000),
(13, 'Bali', 5000),
(14, 'Banten', 5000),
(15, 'DKI', 5000),
(16, 'Jawa Barat', 5000),
(17, 'Jawa Tengah', 5000),
(18, 'Jawa Timur', 5000),
(19, 'DIY', 5000),
(20, 'NTB', 5000);

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(5) UNSIGNED NOT NULL,
  `jenis` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `jenis`) VALUES
(1, 'Fashion'),
(2, 'Kosmetik'),
(3, 'Barang');

-- --------------------------------------------------------

--
-- Table structure for table `kirim`
--

CREATE TABLE `kirim` (
  `id_kirim` int(10) UNSIGNED NOT NULL,
  `id_bayar` int(10) UNSIGNED NOT NULL,
  `tgl_kirim` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kirim`
--

INSERT INTO `kirim` (`id_kirim`, `id_bayar`, `tgl_kirim`) VALUES
(1, 1, '2012-05-01'),
(2, 3, '2012-05-12'),
(3, 5, '2012-05-15'),
(4, 4, '2012-05-15'),
(5, 6, '2012-05-15'),
(6, 9, '0000-00-00'),
(7, 9, '2018-01-09'),
(8, 8, '2018-01-09');

-- --------------------------------------------------------

--
-- Table structure for table `konfirmasi`
--

CREATE TABLE `konfirmasi` (
  `id_konf` int(10) UNSIGNED NOT NULL,
  `id_pesan` varchar(6) NOT NULL,
  `tgl` date DEFAULT NULL,
  `no_bukti` varchar(16) NOT NULL,
  `jum_bayar` int(10) UNSIGNED NOT NULL,
  `sts_baca` enum('0','1') NOT NULL DEFAULT '0',
  `keterangan` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konfirmasi`
--

INSERT INTO `konfirmasi` (`id_konf`, `id_pesan`, `tgl`, `no_bukti`, `jum_bayar`, `sts_baca`, `keterangan`) VALUES
(1, 'P00002', '2012-05-01', 'n001', 102000, '1', ''),
(2, 'P00003', '2012-05-12', 'fgfg', 77000, '1', 'sudah bayar'),
(3, 'P00004', '2012-05-15', '14646045', 62000, '1', 'barang sudah dikirim...silahkan tunggu'),
(4, 'P00005', '2012-05-15', '542875272174', 172000, '1', 'Pembayaran telah diterima...silahkan tunggu..\r\nmungkin barang yang anda pesan dalam perjalanan');

-- --------------------------------------------------------

--
-- Table structure for table `kontak`
--

CREATE TABLE `kontak` (
  `id` tinyint(5) UNSIGNED NOT NULL,
  `nama` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  `komen` text NOT NULL,
  `tgl` date DEFAULT NULL,
  `jam` time DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontak`
--

INSERT INTO `kontak` (`id`, `nama`, `email`, `komen`, `tgl`, `jam`, `status`) VALUES
(1, 'siti khodiyah', 'diyah1912@gmail.com', 'Terima Kasih atas pelayanan berbelanja di Sekido Online Shop. ', '2012-05-15', '06:46:39', '0'),
(2, 'sd', 'sd@s.com', 'sd', '2016-11-03', '15:58:19', '1');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `id_pesan` varchar(6) NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_jarak` int(5) UNSIGNED NOT NULL,
  `tgl` date DEFAULT NULL,
  `keterangan` text,
  `sts_bayar` enum('sudah','belum') NOT NULL DEFAULT 'belum',
  `ttl_bayar` int(10) UNSIGNED NOT NULL,
  `tujuan` varchar(32) NOT NULL,
  `kota` varchar(32) NOT NULL,
  `propinsi` varchar(32) NOT NULL,
  `kd_pos` varchar(12) DEFAULT NULL,
  `telepon` varchar(16) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`id_pesan`, `id_user`, `id_jarak`, `tgl`, `keterangan`, `sts_bayar`, `ttl_bayar`, `tujuan`, `kota`, `propinsi`, `kd_pos`, `telepon`) VALUES
('P00001', 5, 4, '2012-05-01', '', 'belum', 135000, 'Denokan Maguwoharjo Depok Sleman', 'DIY', 'DIY\r\n', '55282', '085224561214'),
('P00002', 5, 3, '2012-05-01', '', 'sudah', 102000, 'Denokan Maguwoharjo Depok Sleman', 'DIY', 'DIY\r\n', '55282', '085224561214'),
('P00003', 8, 3, '2012-05-12', 'gfdgf', 'sudah', 77000, 'sambilegi', 'jogja', 'DIY', '55571', '085643876346'),
('P00004', 8, 3, '2012-05-15', 'ghgh', 'sudah', 62000, 'sambilegi', 'jogja', 'DIY', '55571', '085643876346'),
('P00005', 8, 3, '2012-05-15', '', 'sudah', 172000, 'sambilegi', 'jogja', 'DIY', '55571', '085643876346'),
('P00006', 8, 3, '2012-05-15', '', 'belum', 62000, 'sambilegi', 'jogja', 'DIY', '55571', '085643876346'),
('P00007', 5, 3, '2016-11-03', '', 'belum', 125000, 'Denokan Maguwoharjo Depok Sleman', 'DIY', 'DIY\r\n', '55282', '085224561214'),
('P00008', 9, 4, '2018-01-07', 'ye', 'sudah', 145000, 'bandung', 'ggg', 'Jawa Barat\r\n', '112', '99999');

-- --------------------------------------------------------

--
-- Table structure for table `pesan_detail`
--

CREATE TABLE `pesan_detail` (
  `id_det` int(10) UNSIGNED NOT NULL,
  `id_pesan` varchar(6) NOT NULL,
  `id_produk` int(10) UNSIGNED NOT NULL,
  `harga_satuan` int(10) UNSIGNED NOT NULL,
  `jumlah_barang` tinyint(3) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesan_detail`
--

INSERT INTO `pesan_detail` (`id_det`, `id_pesan`, `id_produk`, `harga_satuan`, `jumlah_barang`) VALUES
(1, 'P00001', 2, 65000, 2),
(2, 'P00002', 10, 50000, 2),
(3, 'P00003', 9, 75000, 1),
(4, 'P00004', 11, 60000, 1),
(5, 'P00005', 12, 85000, 2),
(6, 'P00006', 13, 60000, 1),
(7, 'P00007', 7, 53000, 1),
(8, 'P00007', 8, 70000, 1),
(9, 'P00008', 8, 70000, 2);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(10) UNSIGNED NOT NULL,
  `id_jenis` int(5) UNSIGNED NOT NULL,
  `produk` varchar(64) NOT NULL,
  `harga` int(10) UNSIGNED NOT NULL,
  `berat` varchar(4) NOT NULL,
  `keterangan` text,
  `gambar` varchar(255) DEFAULT NULL,
  `status` enum('ada','tidak') NOT NULL DEFAULT 'ada'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `id_jenis`, `produk`, `harga`, `berat`, `keterangan`, `gambar`, `status`) VALUES
(1, 1, 'Rayon Spandek', 100000, '1', ' ', 'foto_produk/Rayon spandek-orange.jpg', 'ada'),
(2, 1, 'Kemeja Full Print', 100000, '1', ' ', 'foto_produk/Kmj full print cat.jpg', 'ada'),
(3, 1, 'Kaos Spandek', 100000, '1', ' ', 'foto_produk/Kaos spandek.jpg', 'ada'),
(4, 1, 'Soft Jeans', 100000, '1', ' ', 'foto_produk/Softjeans.jpg', 'ada'),
(5, 2, 'Acai Bery', 75000, '1', ' ', 'foto_produk/acai berry@75ribu.jpg', 'ada'),
(6, 2, 'Acai Berry Scrub', 50000, '1', ' Obesitas atau kegemukan dapat terjadi pada siapa saja terutama pada wanita, karna perkembangan hormon pada wanita sangat cepat sehingga menyebapkan beberapa efek samping di antaranya kegemukan, Namun ke gemukan bisa saja ter jadi pada daerah-daerah tertentu saja seperti di lengan, perut, leher, masalah seperti ini jika di atasi dengan pelangsingan yang di konsumsi seperti pil atau kapsul akan kurang efektif. Untuk lebih cepat dan efektif melangsingkan tubuh di bagian tertentu gunakanlah Acai Berry Slimming Whitening Scrub.', 'foto_produk/acaibery scrub@50ribu.jpg', 'ada'),
(7, 2, 'Acno Lution Jerawat', 53000, '1', 'Acnol lotion adalah obat jerawat untuk mengurangi minyak berlebih, serta membantu melawan bakteri penyebab jerawat.\r\nCara Pakai:\r\n- cuci bersih wajah yang berjerawat, keringkan dgn handuk.\r\n- kocok acne lotion sebelum dipakai\r\n- pakai jari untuk mengoleskan acnol pada jerawatm cukup dioles, jangan di gosok.\r\n- pakai 2-3 kali sehari, atau bisa diperpanjang,\r\ntidak menyebabkan ketergantungan. ', 'foto_produk/acno lotion jerawat@53ribu.jpg', 'ada'),
(8, 2, 'Adha Cream + Serum', 70000, '1', 'Cream Herbal (A-DHA Beauty Care) merupakan produk farmasi yang telah lulus uji di Lab UI dan Lab Sucofindo dan dinyatakan aman dan tidak berbahaya. Sudah dipatenkan di HAKI (Hak Kekayaan Intelektual) dan sedang proses sertifikasi LP-POM MUI. ', 'foto_produk/adha cream+serum@70ribu.jpg', 'ada');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `nama` varchar(64) NOT NULL,
  `user` varchar(32) NOT NULL,
  `pass` varchar(32) NOT NULL,
  `alamat` varchar(128) DEFAULT NULL,
  `kota` varchar(32) DEFAULT NULL,
  `propinsi` varchar(32) DEFAULT 'other',
  `sex` enum('p','w') NOT NULL DEFAULT 'p',
  `kd_pos` varchar(12) DEFAULT NULL,
  `telepon` varchar(16) DEFAULT NULL,
  `email` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `user`, `pass`, `alamat`, `kota`, `propinsi`, `sex`, `kd_pos`, `telepon`, `email`) VALUES
(5, 'Pawirol', 'a', '0cc175b9c0f1b6a831c399e269772661', 'Denokan Maguwoharjo Depok Sleman Yogyakarta', 'DIY', 'DIY\r\n', 'p', '55282', '085224561214', 'pawiro@yahoo.com'),
(6, 'b', 'b', '875f26fdb1cecf20ceb4ca028263dec6', 'b', 'b', 'Bengkulu\r\n', 'p', '5', '1', 'a@g.com'),
(7, 'siti khodiyah', 'diyah', 'e10adc3949ba59abbe56e057f20f883e', 'sambilegi, maguwo', 'jogja', 'DIY', 'w', '55571', '085643876346', 'dee_crz90@yahoo.com'),
(8, 'diah', 'diah', '2622115399b7d2eeac4e948a7e2405d2', 'sambilegi', 'jogja', 'DIY', 'w', '55571', '085643876346', 'diah_crz90@yahoo.com'),
(9, 'ikhsan', 'ikhsan', '4e9194a3bb65ab53e41247480905c391', 'godean', 'bantul', 'DIY\r\n', 'p', '344', '09876', 'qwwee@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`user`);

--
-- Indexes for table `bayar`
--
ALTER TABLE `bayar`
  ADD PRIMARY KEY (`id_bayar`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `halaman`
--
ALTER TABLE `halaman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jarak`
--
ALTER TABLE `jarak`
  ADD PRIMARY KEY (`id_jarak`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `kirim`
--
ALTER TABLE `kirim`
  ADD PRIMARY KEY (`id_kirim`);

--
-- Indexes for table `konfirmasi`
--
ALTER TABLE `konfirmasi`
  ADD PRIMARY KEY (`id_konf`);

--
-- Indexes for table `kontak`
--
ALTER TABLE `kontak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id_pesan`);

--
-- Indexes for table `pesan_detail`
--
ALTER TABLE `pesan_detail`
  ADD PRIMARY KEY (`id_det`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `user` (`user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bayar`
--
ALTER TABLE `bayar`
  MODIFY `id_bayar` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id` tinyint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` tinyint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `halaman`
--
ALTER TABLE `halaman`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `jarak`
--
ALTER TABLE `jarak`
  MODIFY `id_jarak` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kirim`
--
ALTER TABLE `kirim`
  MODIFY `id_kirim` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `konfirmasi`
--
ALTER TABLE `konfirmasi`
  MODIFY `id_konf` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kontak`
--
ALTER TABLE `kontak`
  MODIFY `id` tinyint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pesan_detail`
--
ALTER TABLE `pesan_detail`
  MODIFY `id_det` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
