<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	function ambil_gambar($kdProduk) {
		return fetch_row("SELECT gambar FROM produk WHERE id_produk='$kdProduk'");
	}

	function hapus_gambar($kdProduk) {
		$gbr = ambil_gambar($kdProduk);
		if ($gbr!='') {
			$fileGmbr = "../".$gbr;
			if (file_exists($fileGmbr)) unlink($fileGmbr);
		}
	}

	if (isset($_POST['edit_produk'])) {
		$kode   = valid_form($_POST['kode']);
		$jenis  = valid_form($_POST['jenis']);
		$produk = valid_form($_POST['produk']);
		$harga  = valid_form($_POST['harga']);
		$berat	= valid_form($_POST['berat']);
		$ktrngn = valid_form($_POST['keterangan']);
		$status = valid_form($_POST['sts']);
		$gambar = $_FILES['gambar']['name'];

		if (empty($jenis)) {
			pesan_error("Data jenis tidak boleh kosong");

		} else if (empty($produk) && empty($harga) && empty($berat) && empty($ktrngn)) {
			pesan_error("Data produk, harga, berat dan keterangan harus diisi");

		} else if (empty($gambar)) {
			$sql = "UPDATE produk SET status='$status', id_jenis='$jenis', produk='$produk', harga='$harga', berat='$berat', keterangan='$ktrngn' WHERE id_produk='$kode'";
			$qry = query($sql);
			pesan_submit("?act=Produk.Lihat");
			exit;
		
		} else {			
			hapus_gambar($kode);
			$folder = '../foto_produk/'.$gambar;
			if (move_uploaded_file($_FILES['gambar']['tmp_name'], $folder)){
				$file = 'foto_produk/'.$gambar;
			}
			$sql = "UPDATE produk SET status='$status', id_jenis='$jenis', produk='$produk', harga='$harga', berat='$berat', keterangan='$ktrngn', ".			   "gambar='$file' WHERE id_produk='$kode'";
			$qry = query($sql);
			pesan_submit("?act=Produk.Lihat");
			exit;
		}	

	}
	
	require_once head;
	
?>
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Tambah Data User</h3>
			</div>

		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
					<br />
						<?php
							$kde = $_GET['Id'];
							$sql = "SELECT * FROM produk WHERE id_produk='$kde'";
							$qry = query($sql);
							list($id_produk,$id_jenis,$produk,$harga,$berat,$ktrngn,$gambar,$sts) = mysql_fetch_array($qry);
							$cek_ada = cek_ada($sts);
							$cek_tidak = cek_tidak($sts);
						?>
						<form action="" class="form-horizontal form-label-left" method="post" name="edit_produk" enctype='multipart/form-data' onsubmit='return cek_edit_produk()'>
							<input type="hidden" name="kode" value="<?=$id_produk?>">
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Jenis Produk</label>
								<div class="col-md-5 col-sm-10 col-xs-12">
									<select name="jenis" class="form-control"><?=combo_jenis($id_jenis)?></select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Nama Produk</label>
								<div class="col-md-5 col-sm-10 col-xs-12">
									<input name="produk" type="text" class="form-control" maxlength="40" value="<?=$produk?>"/>
								</div>
							</div>
							<div class="form-group">					
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Harga (gunakan angka)</label>
								<div class="col-md-5 col-sm-10 col-xs-12">
									<input name="harga" type="text" class="form-control" maxlength="40" value="<?=$harga?>"/>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Berat (satuan kilogram)</label>
								<div class="col-md-5 col-sm-10 col-xs-12">
									<input name="berat" type="text" class="form-control" maxlength="40" value="<?=$berat?>"/>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Status</label>
								<div class="col-md-5 col-sm-10 col-xs-12">
									<input type="radio" name="sts" value="ada" <?=$cek_ada?> />Ada
									<input type="radio" name="sts" value="tidak" <?=$cek_tidak?> />Tidak
								</div>
							</div>				
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Keterangan</label>
								<div class="col-md-5 col-sm-10 col-xs-12">
									<textarea name="keterangan" class="form-control"><?=$ktrngn?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Gambar</label>
								<div class="col-md-5 col-sm-10 col-xs-12">
									<input name="gambar" type="file" size="37">
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-2">
									<input type="submit" name="edit_produk" class="btn btn-primary" value="Simpan" />
									<input type="reset" name="reset" class="btn btn-danger" value="Batal">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php require_once foot ?>