<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	if (isset($_GET['Del'])) {
		$id  = $_GET['Del'];
		$sql = "DELETE FROM berita WHERE id='$id'";
		$qry = query($sql);
		pesan_delete("?act=Berita.Lihat");
		exit;
	}
	
	require_once head;
	
?>
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Daftar Berita</h3>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<a href="?act=Berita.Tambah" title="Tambah Berita" class="btn btn-info">Tambah Berita</a>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
					<!-- start konten tengah-->
					<div class="center_content">
						
						<!-- start konten kiri-->
						<div class="left_content">
							<div class="left_box">
								<table class="table table-striped">
									<tr>
										<th>No</th>
										<th>Berita</th>
										<th>Action</th>
									</tr>

									<?php
										
										$sql = "SELECT id, berita FROM berita ORDER BY id DESC";
										$cek = num_rows($sql);
										$arr = pager_isi($sql,10);
										$i = no_baris(10);
										
										if ($cek < 1) {
											echo "<tr><td colspan='3'><marquee>Data Berita Masih Kosong..!!</marquee></td></tr>";

										} else {
											foreach ($arr as $baris) { 
												$id = $baris['id'];
												$br = $baris['berita'];
												
												if ($baris[0]!='') {	
													$i++;
													echo "<tr>";
													echo "<td align='right' valign='top'>$i</td><td>".substr($br,0,300)."</td>";
													echo "<td valign='top'>
														<a href='?act=Berita.Detail&Id=$id' class='btn btn-sm btn-info' title='Detail Berita'>Detail</a>
														<a href='?act=Berita.Edit&Id=$id' class='btn btn-sm btn-primary' title='Edit Berita'>Edit</a>
														<a href='?act=Berita.Lihat&Del=$id' class='btn btn-sm btn-danger' title='Hapus Berita'
														onclick='return confirm(\"Yakin data akan dihapus..?\")'>Hapus</a>
														</td>";
													echo "</tr>";
												} 
											} 
										}
									?>

								</table>
					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<ul class="pagination pull-right">
				<?php $path = "?act=Berita.Lihat";pager($sql,10,$path); ?>
				</ul>
			</div>
			<div class="clearfix"></div>

		</div>
	</div>
</div>
<!-- /page content -->

<?php require_once foot ?>