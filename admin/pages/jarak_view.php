<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	if (isset($_GET['Del'])) {
		$id  = $_GET['Del'];
		$sql = "DELETE FROM jarak WHERE id_jarak='$id'";
		$qry = query($sql);
		pesan_delete("?act=Jarak.Lihat");
		exit;
	}
	
	require_once head;
	
?>
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Manajemen <small>Data Jarak</small></h3>
			</div>

			<div class="title_right">
				<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search for...">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button">
								Go!
							</button> </span>
					</div>
				</div>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<a href="?act=Jarak.Tambah" title="Tambah Jarak" class="btn btn-info">Tambah Jarak</a>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="center_content">
							<div class="left_content">
								<div class="left_box">
									<h3>Daftar Jarak</h3>
									<table class="table table-striped">
										<thead>
											<tr>
												<th>No</th>
												<th>Tujuan</th>
												<th>Ongkos</th>
												<th>Action</th>
											</tr											>
										</thead>
										<tbody>
										<?php
											$sql = "SELECT * FROM jarak ORDER BY tujuan ASC";
											$cek = num_rows($sql);
											$arr = pager_isi($sql,10);
											$i = no_baris(10);
											
											if ($cek < 1) {
												echo "<tr><td colspan='5'><marquee>Data Jarak Masih Kosong..!!</marquee></td></tr>";

											} else {
												foreach ($arr as $baris) { 
													$id = $baris['id_jarak'];
													$tj = $baris['tujuan'];
													$ok = $baris['ongkos'];
													
													if ($baris[0]!='') {	
														$i++;
														echo "<tr>";
														echo "<td align='right'>$i</td><td>".ucwords($tj)."</td><td>".format_uang($ok)."</td>";
														echo "<td>
															<a href='?act=Jarak.Edit&Id=$id' class='edit' title='Edit Jarak'>Edit</a>
															<a href='?act=Jarak.Lihat&Del=$id' class='delete' title='Hapus Jarak'
															onclick='return confirm(\"Yakin data akan dihapus..?\")'>Hapus</a>
															</td>";
														echo "</tr>";
													} 
												} 
											}
										?>
										</tbody>
									</table>

					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<ul class="pagination pull-right">
				<?php $path = "?act=Jarak.Lihat";pager($sql,10,$path); ?>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- /page content -->

<?php require_once foot ?>