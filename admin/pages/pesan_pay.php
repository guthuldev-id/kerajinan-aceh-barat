<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');

	if ($_GET['sts']==='delete') {
		$sql = "DELETE FROM bayar WHERE id_bayar='".$_GET['kode']."'";
		$qry = query($sql);
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ?act=Pesan.Bayar'>";
	}
	
	if (isset($_POST['pay'])) {
		$id_psn = $_POST['id_psn'];
		$tgl = $_POST['tgl'];
		$bln = $_POST['bln'];
		$thn = $_POST['thn'];
		$tanggal = $thn."-".$bln."-".$tgl;
		$nobukti = valid_form($_POST['no_bukti']);
		$jumbyar = valid_form($_POST['bayar']);
		
		if (empty($nobukti) || empty($jumbyar)) {
			pesan_error("Data masih ada yang kosong");
			exit;
			
		} else {
			query("UPDATE pesan SET sts_bayar='sudah' WHERE id_pesan='$id_psn'");
			$sql = "INSERT INTO bayar VALUES ('','$id_psn','$tanggal','$nobukti','$jumbyar','belum')"; 
			query ($sql);
			header ("Location: ?act=Pesan.Bayar");

		}
	
	}
		
	require_once head;
	
?>
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Halaman Pembayaran</h3>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<br />
						<?php
							if (($_GET['sts']==='input')) {
								$status = "input";
								$pesan  = $_GET['pesan'];
								$bukti  = $_GET['bukti'];
								$bayar  = $_GET['bayar'];
								$total  = total_bayar($pesan);
								$sql = mysql_query("select * from konfirmasi where id_pesan='$pesan'");
								$dt = mysql_fetch_array($sql);
								$bukti2  = $dt[no_bukti];
								$bayar2  = $dt[jum_bayar];					
						?>
							<form method='post' class="form-horizontal form-label-left" name='form_bayar' action=''>
								<table class="table">
									<tbody>
										<tr>
											<th width="15%">Kode Pesan</th>
											<td>: <?=$pesan?></td>
											<input type="hidden" name="id_psn" value="<?=$pesan?>">
										</tr>
										<tr>
											<th width="15%">Total Pesan</th>
											<td>: <?=format_uang($total)?></td>
										</tr>
										<tr>
											<th width="15%">Tanggal Bayar</th>
											<td>: 
												<select name='tgl' class="tgl"><?php echo combo_tgl(); ?></select>
												<select name='bln' class="bln"><?php echo combo_bln(); ?></select>
												<select name='thn' class="thn"><?php echo combo_thn(); ?></select>
											</td>
										</tr>
										<tr>
											<th width="15%">No Bukti</th>
											<td>: <input type="text" name="no_bukti" class="form_admin" value="<?=$bukti2?>"></td>
										</tr>
										<tr>
											<th width="15%">Bayar</th>
											<td>: <input type="text" name="bayar" class="form_admin" value="<?=$bayar2?>"></td>
										</tr>
									</tbody>
								</table>
								<div class="ln_solid"></div>
								<div class="form-group">
									<div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-2">
										<input type="submit" name="pay" class="btn btn-primary" value="Simpan" />
									</div>
								</div>
							</form>
		
							<?php 
							
								} else {
									echo "<table class=\"table table-striped\">";
									echo "<tr><th>No</th><th>Id Bayar</th><th>Id Pesan</th><th>Tanggal Bayar</th>
									<th>No Bukti</th><th>Jumlah Bayar</th><th>Action</th></tr>";
									
									$sql = "SELECT * FROM bayar WHERE sts_kirim='belum' ORDER BY id_bayar DESC ";
													
									if (num_rows($sql) < 1) {
										echo "<tr><td colspan='8' align='center'><marquee>Data Masih Kosong</marquee></td></tr>";
									
									} else {
										$arr = pager_isi($sql,10);
										$i=no_baris();
										foreach ($arr as $baris) { 
											if ($baris[0]!='') {	
												$i++;
												echo "<tr>";
												echo "<td>$i</td>";
												echo "<td>$baris[0]</td>";
												echo "<td>".ucwords($baris[1])."</td>";
												echo "<td>".convert_tanggal($baris[2])."</td>";
												echo "<td>$baris[3]</td>";
												echo "<td>".format_uang($baris[4])."</td>";
												echo "<td><a href='?act=Pesan.Kirim&sts=input&bayar=$baris[0]&pesan=$baris[1]' 
														class='btn btn-sm btn-info'>Edit</a>";
												echo "<a href='?act=Pesan.Bayar&sts=delete&kode=$baris[0]'
													onclick=\"return confirm('Yakin data akan dihapus');\" title='Hapus Data' class='btn btn-sm btn-danger'>Hapus</a></td>";
												echo "</tr>";
											}
										} 
									}
							?>

								</table>
								
								<div class="left_bg2">
									<?php $path = "?act=Pesan.Bayar";pager($sql,10,$path); ?>
								</div>

							<?php } ?>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php require_once foot ?>