<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	if (isset($_POST['add_faq'])) {
		$tanya = valid_form($_POST['tanya']);
		$jawab = valid_form($_POST['jawab']);
		
		if (!empty($tanya) && !empty($jawab)) {
			$sql = "INSERT INTO faq (tanya,jawab) VALUES ('$tanya','$jawab')";
			$qry = query($sql);
			pesan_submit("?act=Faq.Lihat");
			exit;
		}
	}
	
	require_once head;
	
?>
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Tambah Data FAQ</h3>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<br />
						<form action="" class="form-horizontal form-label-left" method="post" name="form_add_faq" onsubmit='return cek_add_faq()'>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Pertanyaan</label>
								<div class="col-md-5 col-sm-10 col-xs-12">
									<textarea name="tanya" class="form-control" rows="5"></textarea>
								</div>
							</div>  
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Jawaban</label>
								<div class="col-md-5 col-sm-10 col-xs-12">
									<textarea name="jawab" class="form-control" rows="5"></textarea>
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-2">
									<input type="submit" name="add_faq" class="btn btn-primary" value="Simpan" />
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php require_once foot ?>