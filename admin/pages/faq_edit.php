<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	if (isset($_POST['edit_faq'])) {
		$kode  = $_POST['kode'];
		$tanya = valid_form($_POST['tanya']);
		$jawab = valid_form($_POST['jawab']);
		
		if (!empty($tanya) && !empty($jawab)) {
			$sql = "UPDATE faq SET tanya='$tanya', jawab='$jawab' WHERE id='$kode'";
			$qry = query($sql);
			pesan_submit("?act=Faq.Lihat");
			exit;
		}
	}
	
	require_once head;
	
?>
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Edit Data FAQ</h3>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<br />
						<?php
							$id = $_GET['Id'];
							$sql = "SELECT * FROM faq WHERE id='$id'";
							$qry = query($sql);
							list($kode,$tanya,$jawab) = mysql_fetch_array($qry);
						?>
						<form action="" method="post" class="form-horizontal form-label-left" name="form_edit_faq" onsubmit='return cek_edit_faq()'>
							<input type="hidden" name="kode" value="<?=$kode?>">
							<div class="row">
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Pertanyaan</label>
								<div class="col-md-5 col-sm-10 col-xs-12">
									<textarea name="tanya" class="form-control" rows="5"><?=$tanya?></textarea>
								</div>
							</div>  
							<div class="row">
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Jawaban</label>
								<div class="col-md-5 col-sm-10 col-xs-12">
									<textarea name="jawab" class="form-control" rows="5"><?=$jawab?></textarea>
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-2">
									<input type="submit" name="edit_faq" class="btn btn-primary" value="Simpan" />
									<input type="reset" name="reset" class="btn btn-danger" value="Batal" />
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php require_once foot ?>