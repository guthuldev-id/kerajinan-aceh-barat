<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	if (isset($_GET['Del'])) {
		$id  = $_GET['Del'];
		$sql = "DELETE FROM jenis WHERE id_jenis='$id'";
		$qry = query($sql);
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ?act=Jenis.Lihat'>";
		exit;
	}
	
	require_once head;
	
?>

	<!-- start konten tengah-->
	<div class="center_content">
		
		<!-- start konten kiri-->
		<div class="left_content">
			<div class="left_box">
				<h3>Daftar Data Jenis</h3>
				<table class="style1" width="98%">
					<tr><th align="right" width="20">No</th><th width="480px">Jenis</th><th width="30px">Action</th></tr>

	<?php
		
		$sql = "SELECT * FROM jenis ORDER BY id_jenis DESC";
		$cek = num_rows($sql);
		$arr = pager_isi($sql,10);
		$i = no_baris(10);
		
		if ($cek < 1) {
			echo "<tr><td colspan='3'><marquee>Data Jenis Masih Kosong..!!</marquee></td></tr>";

		} else {
			foreach ($arr as $baris) { 
				$kode = $baris['id_jenis'];
				$jnis = $baris['jenis'];

				if ($baris[0]!='') {	
					$i++;
					echo "<tr>";
					echo "<td align='right'>$i</td>";
					echo "<td>".ucwords($jnis)."</td>";
					echo "<td valign='top'>
						<a href='?act=Jenis.Edit&Id=$kode' class='edit' title='Edit Jenis'</a>
						<a href='?act=Jenis.Lihat&Del=$kode' class='delete' title='Hapus Jenis'
						onclick='return confirm(\"Yakin data akan dihapus..?\")'></a>
						</td>";
					echo "</tr>";
				} 
			} 
		}
	?>

				</table>
				<div class="left_bg2"><?php $path = "?act=Produk.Lihat";pager($sql,10,$path); ?></div>
			</div>
		</div>
		<!--end konten kiri-->

		<!-- start konten kanan-->
		<div class="right_content">
			<div class="right_box">
				
				<h3>Control Panel</h3>
				<div class="box_right_list">
				<ul class="right_list">
					<li><a href="?act=Jenis.Tambah" class="news">Tambah Data Jenis</a></li>
					<li><a href="?act=Produk.Lihat" class="news">Olah Data Produk</a></li>
				</ul>
				</div>

			</div>
		</div>
		<!--end konten kanan-->
	   
		<div class="clear"></div>
	
	</div>
	<!--end konten tengah-->

<?php require_once foot ?>