<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	if (isset($_GET['Del'])) {
		$id  = $_GET['Del'];
		$sql = "DELETE FROM kontak WHERE id='$id'";
		$qry = query($sql);
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ?act=Kontak.Komen'>";
		exit;
	}

	if (isset($_GET['Accept'])) {
		$id  = $_GET['Accept'];
		$sql = "UPDATE kontak SET status='1' WHERE id='$id'";
		$qry = query($sql);
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ?act=Kontak.Komen'>";
		exit;
	}

	if (isset($_GET['Delete'])) {
		$sql = "DELETE FROM kontak WHERE status='0'";
		$qry = query($sql);
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ?act=Kontak.Komen'>";
		exit;
	}
	
	require_once head;
	
?>
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Daftar Komentar Baru</h3>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<div class="center_content">
							<div class="left_content">
								<div class="left_box">
									<table class="table table-striped">
										<tr>
											<th>Komentar</th>
											<th>Action</th>
										</tr>

	<?php
		
		$sql = "SELECT * FROM kontak WHERE status='0' ORDER BY id DESC";
		$cek = num_rows($sql);
		$arr = pager_isi($sql,10);
		
		if ($cek < 1) {
			echo "<tr><td colspan='2'><marquee>Data Komentar Baru Masih Kosong..!!</marquee></td></tr>";

		} else {
			foreach ($arr as $baris) { 
				$id    = $baris['id'];
				$nama  = $baris['nama'];
				$mail  = $baris['email'];
				$komen = $baris['komen'];
				$jam   = $baris['jam'];
				$tgl   = $baris['tgl'];
				
				if ($baris[0]!='') {	
					$i++;
					echo "<tr>";
					echo "<td><div class='left_bg3'>
						<a href='mailto:$mail' class='user'>".ucwords($nama)."</a>&nbsp;&nbsp;&nbsp;
						<a class='time'>".convert_tanggal($tgl)." - $jam</a><br />
						".ucwords(nl2br($komen))."</div></td>";
					echo "<td valign='top'>
						<a href='?act=Kontak.Komen&Accept=$id' class='btn btn-sm btn-success' title='Setujui Komentar'>Setujui</a>
						<a href='?act=Kontak.Komen&Del=$id' class='btn btn-sm btn-danger' title='Hapus Komentar'
						onclick='return confirm(\"Yakin data akan dihapus..?\")'>Hapus</a>
						</td>";
					echo "</tr>";
				} 
			} 
		}
	?>

				</table>
					<a href="?act=Kontak.Lihat" title="Daftar Komentar" class="btn btn-info">Daftar Komentar</a>
					<div class="left_bg1"><a href='?act=Kontak.Komen&Delete' class='btn btn-danger' title='Hapus Semua Komentar' onclick='return confirm("Yakin semua komentar akan dihapus..?")'>Hapus Seluruh Komentar Baru</a></div>
					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<ul class="pagination pull-right">
					<?php $path = "?act=Kontak.Komen";pager($sql,10,$path); ?>
				</ul>
			</div>
			<div class="clearfix"></div>

		</div>
	</div>
</div>
<?php require_once foot ?>