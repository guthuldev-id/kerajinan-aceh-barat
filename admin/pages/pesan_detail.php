<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');

	require_once head;

	$sql = "SELECT id_pesan, id_jarak FROM pesan WHERE id_pesan='".$_GET['Id']."'";
	list($kd_pesan,$kd_jasa) = mysql_fetch_row(query($sql));
	$tarif = tarif_jarak($kd_jasa);

	$sql = "SELECT u.nama, p.tujuan, p.kota, p.propinsi, p.kd_pos, p.telepon, u.email ".
		   "FROM user AS u, pesan AS p ".
		   "WHERE p.id_user=u.id_user AND p.id_pesan='$kd_pesan'";
	$row = mysql_fetch_row(query($sql));
	
?>
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Halaman Pemesanan</h3>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<div class="center_content">
							<div class="left_content">
								<div class="left_box">
									<table class="table table-striped">
				
	<?php
							
		echo "<tr><th colspan='6'>Pemesanan anda berakhir pada tanggal : ".convert_tanggal(kadaluarsa($_GET['Tgl']))."</th></tr>";
		echo "<tr><th>No</th><th>Nama Produk</th><th>Harga</th><th>Berat</th><th>Qty</th><th>Total</th></tr>";

		$sql = "SELECT id_pesan, id_jarak FROM pesan WHERE id_pesan='".$kd_pesan."'";
		list($kd_pesan,$kd_jasa) = mysql_fetch_row(query($sql));
		$tarif = tarif_jarak($kd_jasa);
			
		$sql = "SELECT p.produk, d.jumlah_barang, d.harga_satuan, (p.berat * d.jumlah_barang) AS sberat, ".
			   "(d.jumlah_barang * d.harga_satuan) AS stotal ".
		       "FROM produk AS p, pesan_detail AS d ".
		       "WHERE d.id_produk=p.id_produk AND d.id_pesan='$kd_pesan'";
		$isi = query($sql);
		$no=0;
		while (list($nama,$jum,$harga,$sberat,$stotal) = mysql_fetch_row($isi))	{	
			$no++;		
			$total += $stotal;
			$berat += $sberat;

			echo "<tr $no>";
			echo "<td align='center'>".$no."</td>";
			echo "<td align='left'>".ucwords($nama)."</td>";
			echo "<td align='right'>".format_uang($harga)."</td>";
			echo "<td align='center'>".$sberat."Kg</td>";
			echo "<td align='center'>".$jum."</td>";
			echo "<td align='right'>".format_uang($stotal)."</td>";
			echo "</tr>";
		} 
		
		if ($no>0) {
			//$sberat = ceil($berat);
			//$starif = $tarif*$sberat;
			$skabeh = $total+$tarif;
			echo "<tr><td colspan='6' align='right'>
				<strong>Total Belanja Anda : </strong>".format_uang($total)."</td></tr>";
			echo "<tr><td colspan='6' align='right'>
				<strong>Total Ongkos Kirim : </strong>".format_uang($tarif)."</td></tr>";
			echo "<tr><td colspan='6' align='right'>
				<strong>Total Keseluruhan : </strong>".format_uang($skabeh)."</td></tr>";
		}
		
		echo "</table>";
		
		echo "<div class='left_bg2'><strong>Terbilang : </strong>";
		echo terbilang($skabeh,3);
		echo "</div>";

		echo "<table class='table'>";
		echo "<tr><td width='150px' align='right'><strong>Atas Nama :</strong> 
			</td><td>".ucwords($row[0])."</td></tr>";
		echo "<tr><td width='120px' align='right'><strong>Alamat Tujuan :</strong> 
			</td><td>".ucwords($row[1])."</td></tr>";
		echo "<tr><td width='120px' align='right'><strong>Kota Tujuan :</strong> 
			</td><td>".ucwords($row[2])."</td></tr>";
		echo "<tr><td width='120px' align='right'><strong>Propinsi Tujuan :</strong> 
			</td><td>".ucwords($row[3])."</td></tr>";
		echo "<tr><td width='120px' align='right'><strong>Kode Pos Tujuan :</strong> 
			</td><td>".ucwords($row[4])."</td></tr>";
		echo "<tr><td width='120px' align='right'><strong>Telepon Pemesan :</strong> 
			</td><td>".ucwords($row[5])."</td></tr>";
		echo "<tr><td width='120px' align='right'><strong>Email Pemesan :</strong> 
			</td><td>$row[6]</td></tr>";
		echo "</table>";

	?>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- /page content -->
<?php require_once foot ?>