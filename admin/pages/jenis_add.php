<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	if (isset($_POST['add_jenis'])) {
		$jenis = valid_form($_POST['jenis']);
		
		if (!empty($jenis)) {
			$sql = "INSERT INTO jenis (jenis) VALUES ('$jenis')";
			$qry = query($sql);
			pesan_submit("?act=Jenis.Lihat");
			exit;
		}	
	}
	
	require_once head;
	
?>

	<!-- start konten tengah-->
	<div class="center_content">
		
		<!-- start konten kiri-->
		<div class="left_content">
			<div class="left_box">
				<h3>Tambah Jenis</h3>
				
				<div class="admin_form">
					<form action="" method="post" name="form_add_jenis" onsubmit='return cek_add_jenis()'>
					<div class="row">
						<label class="form_admin"><strong>Jenis Produk :</strong></label>
						<input name="jenis" type="text" class="form_admin" maxlength="40" />
					</div>
					<div class="row">
					<div style="margin:5px 0 0 205px;">
						<input type="submit" name="add_jenis" class="submit" value="Simpan" />
						<input type="reset" name="reset" class="submit" value="Batal" />
					</div>
					</div>
					</form>
				</div>

			</div>
		</div>
		<!--end konten kiri-->
		
		<!-- start konten kanan-->
		<div class="right_content">
			<div class="right_box">
				
				<h3>Control Panel</h3>
				<div class="box_right_list">
				<ul class="right_list">
					<li><a href="?act=Jenis.Lihat" title="Daftar Jenis" class="news">Daftar Jenis</a></li>
				</ul>
				</div>

			</div>
		</div>
		<!--end konten kanan-->
	   
		<div class="clear"></div>
	
	</div>
	<!--end konten tengah-->

<?php require_once foot ?>