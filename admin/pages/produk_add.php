<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	if (isset($_POST['add_produk'])) {
		$jenis  = valid_form($_POST['jenis']);
		$produk = valid_form($_POST['produk']);
		$harga  = valid_form($_POST['harga']);
		$berat	= valid_form($_POST['berat']);
		$ktrngn = valid_form($_POST['keterangan']);
		$status = valid_form($_POST['sts']);
		$gambar = $_FILES['gambar']['name'];

		if (empty($jenis)) {
			pesan_error("Data jenis tidak boleh kosong");

		} else if (empty($produk) && empty($harga) && empty($berat) && empty($ktrngn)) {
			pesan_error("Data produk, harga, berat dan keterangan harus diisi");

		} else if (empty($gambar)) {
			$sql = "INSERT INTO produk (id_jenis,produk,harga,berat,status,keterangan) ".
				   "VALUES ('$jenis','$produk','$harga','$berat','$status','$ktrngn')";
			$qry = query($sql);
			pesan_submit("?act=Produk.Lihat");
			exit;
		
		} else {			
			$folder = '../foto_produk/'.$gambar;
			if (move_uploaded_file($_FILES['gambar']['tmp_name'], $folder)){
				$file = 'foto_produk/'.$gambar;
			}
			$sql = "INSERT INTO produk (id_jenis,produk,harga,berat,status,keterangan,gambar) ".
				   "VALUES ('$jenis','$produk','$harga','$berat','$status','$ktrngn','$file')";
			$qry = query($sql);
			pesan_submit("?act=Produk.Lihat");
			exit;
		}	

	}
	
	require_once head;
	
?>
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Tambah Data Produk</h3>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<br />
						<form action="" class="form-horizontal form-label-left" method="post" name="add_produk" enctype='multipart/form-data' onsubmit='return cek_add_produk()'>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Jenis Produk</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
								<select name="jenis" class="form-control"><?=combo_jenis()?></select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Nama Produk</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
								<input name="produk" type="text" class="form-control" maxlength="40" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Harga (gunakan angka)</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
								<input name="harga" type="text" class="form-control" maxlength="40" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Berat (satuan kilogram)</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
								<input name="berat" type="text" class="form-control" maxlength="40" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Status</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
								<input type="radio" name="sts" value="ada" checked />Ada
								<input type="radio" name="sts" value="tidak" />Tidak
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Keterangan</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
								<textarea name="keterangan" class="form-control"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Gambar</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
								<input name="gambar" type="file" size="37">
							</div>
						</div>
						<div class="ln_solid"></div>
		                      <div class="form-group">
		                        <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-2">
									<input type="submit" name="add_produk" class="btn btn-primary" value="Simpan" />
									<input type="reset" name="reset" class="btn btn-danger" value="Batal">
		                        </div>
		                      </div>
						</form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


<?php require_once foot ?>