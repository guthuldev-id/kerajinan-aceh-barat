<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');

	if ($_GET['sts']==='delete') {
		$sql = "DELETE FROM pesan WHERE id_pesan='".$_GET['kode']."'";
		$qry = query($sql);
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ?act=Pesan.Baru'>";
	}

	require_once head;
	
?>
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Halaman Pemesanan</h3>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<div class="center_content">
							<div class="left_content">
								<div class="left_box">
									<table class="table table-striped">
										<tr>
											<th>No</th>
											<th>Id Pesan</th>
											<th>Nama User</th>
											<th>Tanggal</th>
											<th>Action</th>
										</tr>
										<?php
											
											$sql = "SELECT p.id_pesan, u.nama, p.tgl FROM pesan AS p, user AS u ".
												   "WHERE p.id_user=u.id_user AND sts_bayar='belum' ORDER BY id_pesan DESC";
											$cek = num_rows($sql);
											$arr = pager_isi($sql,10);
											$i = no_baris(10);
											
											if ($cek < 1) {
												echo "<tr><td colspan='5'><marquee>Data Pesan Masih Kosong..!!</marquee></td></tr>";

											} else {
												foreach ($arr as $baris) { 
													$kode = $baris['id_pesan'];
													$user = $baris['nama'];
													$tgl  = $baris['tgl'];
													
													if ($baris[0]!='') {	
														$i++;
														echo "<tr>";
														echo "<td align='right'>$i</td><td>$kode</td><td>".ucwords($user)."</td><td>".convert_tanggal($tgl)."</td>";
														echo "<td>
														<a href='?act=Pesan.Detail&Id=$kode&Tgl=$tgl' title='Detail Pemesanan' class='btn btn-sm btn-primary'>Detail</a>
														<a href='?act=Pesan.Bayar&sts=input&pesan=$baris[0]' title='Simpan Data Pesan' class='btn btn-sm btn-info'>Simpan</a>
														<a href='?act=Pesan.Baru&sts=delete&kode=$baris[0]' class='btn btn-sm btn-danger' title='Hapus Data onclick='return confirm(\"Yakin data akan dihapus..?\")'>Hapus</a>
														</td>";
														echo "</tr>";
													} 
												} 
											}
										?>
						</table>
					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<ul class="pagination pull-right">
				<?php $path = "?act=Pesan.Baru";pager($sql,10,$path); ?>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- /page content -->
<?php require_once foot ?>