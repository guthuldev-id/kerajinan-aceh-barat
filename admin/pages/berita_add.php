<?php 
	
	defined('RONAL') or die('<b>Cannot Access..!!</b>'); 
	require_once spaw.'spaw.inc.php';

	if (isset($_POST['save'])) {
		$isi = $_POST['isi'];
		$sql = "INSERT INTO berita (berita,tgl,jam) VALUES ('$isi', now(), now())";
		query($sql);
		pesan_submit("?act=Berita.Lihat");
		exit;
	}

	require_once head;

?>
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Tambah Berita</h3>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<br />
						<?php

						echo "<form method='post' class=\"form-horizontal form-label-left\" name='fHalaman'>";
						$editor = new SpawEditor("isi");
						$editor->showResizingGrip();
						$editor->show();
						echo "<p><br><input type='submit' class='btn btn-info' name='save' value='Simpan' 
						onclick='return confirm(\"Yakin data akan disimpan..?\")'></p></form>";

						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php require_once foot ?>