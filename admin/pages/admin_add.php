<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	if (isset($_POST['add_admin'])) {
		$user  = valid_form($_POST['user']);
		$pass1 = valid_form($_POST['pass1']);
		$pass2 = valid_form($_POST['pass2']);
		
		if (cek_user_admin($user)) {
			pesan_error("Username ini sudah terpakai, gunakan user yang lain nya");
		
		} else if (strlen($pass1) < 6) {
			pesan_error("Minimal panjang karakter password adalah 6 karakter");
			
		} else if ($pass1 != $pass2) {
			pesan_error("Password Pertama dan Password Kedua anda tidak sama");
				
		} else if (!empty($user) && !empty($pass1) && !empty($pass2)) {
			$sql = "INSERT INTO admin (user,pass) VALUES ('$user',md5('$pass1'))";
			$qry = query($sql);
			pesan_submit("?act=Admin.Lihat");
			exit;

		}
	}
	
	require_once head;
	
?>

<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Tambah Data Administrator</h3>
			</div>

		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<br />
					<form action="" method="post" class="form-horizontal form-label-left" name="form_add_admin" onsubmit='return cek_add_admin()'>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Username</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
								<input name="user" type="text" maxlength="40" class="form-control" />
							</div>
						</div>  
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Password (Minimal 6 Karakter)</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
								<input name="pass1" type="password" class="form-control" maxlength="40" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Konfirmasi Password</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
								<input name="pass2" type="password" class="form-control" maxlength="40" />
							</div>
						</div>
						<div class="ln_solid"></div>
	                      <div class="form-group">
	                        <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-2">
	                         <input type="submit" name="add_admin" class="btn btn-primary" value="Simpan" />
	                        </div>
	                      </div>
					</form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

<?php require_once foot ?>