<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	if (isset($_POST['edit_user'])) {
		$kode  = $_POST['kode'];
		$nama  = valid_form($_POST['nama']);
		$user  = valid_form(strtolower($_POST['user']));
		$pass1 = valid_form($_POST['pass1']);
		$pass2 = valid_form($_POST['pass2']);
		$mail  = valid_form($_POST['mail']);
		$almt  = valid_form($_POST['almt']);
		$kota  = valid_form($_POST['kota']);
		$kdpos = valid_form($_POST['pos']);
		$tlp   = valid_form($_POST['tlp']);
		$sex   = valid_form($_POST['jk']);
		$prop  = valid_form($_POST['prop']);

		if (!cek_email) {
			pesan_error("Email tidak valid, contoh : your@email.com");	

		} else if (!empty($user) && empty($pass1) && empty($pass2) && !empty($nama) && !empty($mail) && !empty($almt) && 
				  !empty($kota) && !empty($kdpos) && !empty($tlp) && !empty($sex) && !empty($prop)) {
			$sql = "UPDATE user SET user='$user', email='$mail', alamat='$almt', kota='$kota', sex='$sex', ".
				   "sex='$sex', nama='$nama', kd_pos='$kdpos', telepon='$tlp', propinsi='$prop' WHERE id_user='$kode'";
			$qry = query($sql);
			pesan_submit("?act=User.Lihat");
			exit;

		} else if (!empty($user) && !empty($pass1) && !empty($pass2) && !empty($nama) && !empty($mail) && !empty($almt) && 
				  !empty($kota) && !empty($kdpos) && !empty($tlp) && !empty($sex) && !empty($prop)) {
			
			if (strlen($pass1) < 6) {
				pesan_error("Minimal panjang karakter password adalah 6 karakter");
			} else if ($pass1 != $pass2) {
				pesan_error("Password Pertama dan Password Kedua anda tidak sama");
			} else {
				$sql = "UPDATE user SET user='$user', pass=md5('$pass1'), email='$mail', alamat='$almt', kota='$kota', ".
					   "sex='$sex', nama='$nama', kd_pos='$kdpos', telepon='$tlp', propinsi='$prop' WHERE id_user='$kode'";
				$qry = query($sql);
				pesan_submit("?act=User.Lihat");
				exit;
			}
		}
	}
	
	$id  = $_GET['Id'];
	$sql = "SELECT * FROM user WHERE id_user='$id'";
	$qry = query($sql);
	list($id,$nama,$user,$pass,$almt,$kota,$prop,$sex,$kdpos,$tlp,$mail) = mysql_fetch_array($qry);
	$cek_cowo = cek_cowo($sex);
	$cek_cewe = cek_cewe($sex);

	require_once head;
	
?>
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Tambah Data User</h3>
			</div>

		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<br />
					<form action="" class="form-horizontal form-label-left" method="post" name="form_edit_user" onsubmit='return cek_edit_user()'>
						<input type="hidden" name="kode" value="<?=$id?>">
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Nama User</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
								<input name="nama" type="text" maxlength="40" value="<?=$nama?>" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Jenis Kelamin</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
								<input type="radio" name="jk" value="P" <?=$cek_cowo?> />Pria
								<input type="radio" name="jk" value="W" <?=$cek_cewe?> />Wanita
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Alamat</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
								<input name="almt" type="text" class="form-control" value="<?=$almt?>" maxlength="40" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Kota</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
								<input name="kota" type="text" class="form-control" maxlength="40" value="<?=$kota?>" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Propinsi</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
								<select name="prop" class="form-control"><?=combo_propinsi($prop)?></select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Kode Pos</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
								<input name="pos" type="text" class="form-control" maxlength="40" value="<?=$kdpos?>" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">No Telepon</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
								<input name="tlp" type="text" class="form-control" maxlength="40" value="<?=$tlp?>" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Email</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
							<input name="mail" type="text" class="form-control" maxlength="40" value="<?=$mail?>" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Username</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
							<input name="user" type="text" class="form-control" maxlength="40" value="<?=$user?>" />
							</div>
						</div>  
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Password (Minimal 6 Karakter)</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
							<input name="pass1" type="password" class="form-control" maxlength="40" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Konfirmasi Password</label>
							<div class="col-md-5 col-sm-10 col-xs-12">
							<input name="pass2" type="password" class="form-control" maxlength="40" />
							</div>
						</div>
						<div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-2">
								<input type="submit" name="edit_user" class="btn btn-primary" value="Simpan" />
								<input type="reset" name="reset" class="btn btn-danger" value="Batal">
							</div>
						</div>
					</form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


<?php require_once foot ?>