<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	if (isset($_GET['Del'])) {
		$id  = $_GET['Del'];
		$sql = "DELETE FROM faq WHERE id='$id'";
		$qry = query($sql);
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ?act=Faq.Lihat'>";
		exit;
	}

	require_once head;
	
?>
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Daftar Data FAQ</h3>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<a href="?act=Faq.Tambah" title="Tambah Faq" class="btn btn-info">Tambah Faq</a>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="center_content">
							<div class="left_content">
								<div class="left_box">
									<table class="table table-striped">
										<tr>
											<th width="480px">Tanya - Jawab</th>
											<th width="30px">Action</th>
										</tr>
	<?php
		
		$sql = "SELECT * FROM faq ORDER BY id DESC";
		$cek = num_rows($sql);
		$arr = pager_isi($sql,10);
		
		if ($cek < 1) {
			echo "<tr><td colspan='2'><marquee>Data FAQ Masih Kosong..!!</marquee></td></tr>";

		} else {
			foreach ($arr as $baris) { 
				$id     = $baris['id'];
				$tanya  = $baris['tanya'];
				$jawab  = $baris['jawab'];
				
				if ($baris[0]!='') {	
					$i++;
					echo "<tr>";
					echo "<td><div class='left_bg3'>
						<a class='edit'>Pertanyaan</a><br />
						".ucwords(nl2br($tanya))."<br /><br />
						<a class='message'>Jawaban</a><br />
						".ucwords(nl2br($jawab))."
						</div></td>";
					echo "<td valign='top'>
						<a href='?act=Faq.Edit&Id=$id' class='btn btn-sm btn-info' title='Edit FAQ'>Edit</a>
						<a href='?act=Faq.Lihat&Del=$id' class='btn btn-sm btn-danger' title='Hapus FAQ'
						onclick='return confirm(\"Yakin data akan dihapus..?\")'>Delete</a>
						</td>";
					echo "</tr>";
				} 
			} 
		}
	?>

						</table>
					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<ul class="pagination pull-right">
				<?php $path = "?act=Faq.Lihat";pager($sql,10,$path); ?>
				</ul>
			</div>
			<div class="clearfix"></div>

		</div>
	</div>
</div>
<!-- /page content -->

<?php require_once foot ?>