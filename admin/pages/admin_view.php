<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	if (isset($_GET['Del'])) {
		$usr = $_GET['Del'];
		$sql = "DELETE FROM admin WHERE user='$usr'";
		$qry = query($sql);
		pesan_delete("?act=Admin.Lihat");
		exit;
	}
	
	require_once head;
	
?>

	<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Manajemen <small>Data Admin</small></h3>
			</div>

			<div class="title_right">
				<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search for...">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button">
								Go!
							</button> </span>
					</div>
				</div>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">

			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<a href="?act=Admin.Tambah" title="Tambah Administrator" class="btn btn-info">Tambah Administrator</a>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<table class="table table-striped">
							<thead>
								<tr>
									<th width="20px" align="right">No</th>
									<th>Nama</th>
									<th width="40px">Hapus</th>
								</tr>
							</thead>
							<tbody>
							<?php
								
								$sql = "SELECT user FROM admin ORDER BY user ASC";
								$qry = query($sql);
								$i = 0;
								while ($row = mysql_fetch_array($qry)) {
									$i++;
									echo "<tr><td align='right'>$i</td><td>".ucwords($row[0])."</td>
										<td align='center'>
											<a href='?act=Admin.Lihat&Del=$row[0]' class='delete' title='Hapus Data' onclick='return confirm(\"Yakin data akan dihapus..?\")'>Hapus</a>
										</td></tr>";
								}

							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- /page content -->

<?php require_once foot ?>