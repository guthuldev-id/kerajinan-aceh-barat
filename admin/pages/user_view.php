<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	if (isset($_GET['Del'])) {
		$id  = $_GET['Del'];
		$sql = "DELETE FROM user WHERE id_user='$id'";
		$qry = query($sql);
		pesan_delete("?act=User.Lihat");
		exit;
	}
	
	require_once head;
	
?>

<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Manajemen <small>Data User</small></h3>
			</div>

			<div class="title_right">
				<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search for...">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button">
								Go!
							</button> </span>
					</div>
				</div>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<a href="?act=User.Tambah" title="Tambah User" class="btn btn-info">Tambah User</a>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
					<!-- start konten tengah-->
	<div class="center_content">
		
		<!-- start konten kiri-->
		<div class="left_content">
			<div class="left_box">
				<h3>Daftar User</h3>
				<table class="table table-striped">
					<tr><th>No</th><th>Nama</th><th>User</th>
					<th width="80px">Gender</th><th>Action</th></tr>

	<?php
		
		$sql = "SELECT id_user, nama, user, sex FROM user ORDER BY nama ASC";
		$cek = num_rows($sql);
		$arr = pager_isi($sql,10);
		$i = no_baris(10);
		
		if ($cek < 1) {
			echo "<tr><td colspan='5'><marquee>Data User Masih Kosong..!!</marquee></td></tr>";

		} else {
			foreach ($arr as $baris) { 
				$id = $baris['id_user'];
				$nm = $baris['nama'];
				$us = $baris['user'];
				$jk = $baris['sex'];
				$jk = cek_sex($jk);
				
				if ($baris[0]!='') {	
					$i++;
					echo "<tr>";
					echo "<td align='right'>$i</td><td>".ucwords($nm)."</td><td>".ucfirst($us)."</td><td align='center'>$jk</td>";
					echo "<td>
						<a href='?act=User.Detail&Id=$id' class='detail' title='Detail User'>Detail</a>
						<a href='?act=User.Edit&Id=$id' class='edit' title='Edit User'>Edit</a>
						<a href='?act=User.Lihat&Del=$id' class='delete' title='Hapus User' onclick='return confirm(\"Yakin data akan dihapus..?\")'>Hapus</a>
						</td>";
					echo "</tr>";
				} 
			} 
		}
	?>

				</table>

					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<ul class="pagination pull-right">
				<?php $path = "?act=User.Lihat";pager($sql,10,$path); ?>
				</ul>
			</div>
			<div class="clearfix"></div>

		</div>
	</div>
</div>
<!-- /page content -->

<?php require_once foot ?>