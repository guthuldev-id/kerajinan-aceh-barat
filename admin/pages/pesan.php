<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');

	require_once head;
	
?>
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Halaman Proses Pemesanan</h3>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Pemesanan</h3>
					</div>
					<div class="panel-body">
						Menu pemesanan digunakan untuk pengecekan data pemesanan baru yang dilakukan oleh user juga proses input data bayar pemesanan secara manual oleh administrator.
					</div>
					<div class="panel-footer">
						<a class="btn btn-primary btn-sm" href="?act=Pesan.Baru">Lihat</a>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Konfirmasi</h3>
					</div>
					<div class="panel-body">
						Menu konfirmasi digunakan untuk memproses data konfirmasi bayar yang dilakukan oleh user terhadap pemesanan user.
					</div>
					<div class="panel-footer">
						<a class="btn btn-primary btn-sm" href="?act=Pesan.Konfirmasi">Lihat</a>
					</div>
				</div>

				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Pembayaran</h3>
					</div>
					<div class="panel-body">
						Menu pembayaran digunakan untuk proses input data bayar pesanan user dan mengubah status bayar user menjadi sudah bayar.
					</div>
					<div class="panel-footer">
						<a class="btn btn-primary btn-sm" href="?act=Pesan.Bayar">Lihat</a>
					</div>
				</div>
				
				<div class="panel panel-warning">
					<div class="panel-heading">
						<h3 class="panel-title">Pengiriman</h3>
					</div>
					<div class="panel-body">
						Menu pengiriman digunakan untuk proses input data pengiriman barang pesanan dan mengubah status kirim barang pesanan menjadi sudah dikirim.
					</div>
					<div class="panel-footer">
						<a class="btn btn-primary btn-sm" href="?act=Pesan.Kirim">Lihat</a>
					</div>
				</div>
				
				<div class="panel panel-danger">
					<div class="panel-heading">
						<h3 class="panel-title">Laporan</h3>
					</div>
					<div class="panel-body">
						Menu pemesanan digunakan untuk pencetakan data.
					</div>
					<div class="panel-footer">
						<a class="btn btn-primary btn-sm" href="javascript:" onclick="window.open(
						'report.php','newWin','resizable=yes,menubars=no,scrollbars=yes,width=700 height=500'); return false" target="_BLANK" title="Laporan">Lihat</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
				

<?php require_once foot ?>