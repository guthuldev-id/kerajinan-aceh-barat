<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	if (isset($_GET['Del'])) {
		$id  = $_GET['Del'];
		$sql = "DELETE FROM kontak WHERE id='$id'";
		$qry = query($sql);
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ?act=Kontak.Lihat'>";
		exit;
	}
	
	if (isset($_GET['Delete'])) {
		$sql = "DELETE FROM kontak WHERE status='1'";
		$qry = query($sql);
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ?act=Kontak.Lihat'>";
		exit;
	}

	require_once head;
	
?>
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Daftar Komentar Disetujui</h3>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<div class="center_content">
							<div class="left_content">
								<div class="left_box">
									<table class="table table-striped">
										<tr>
											<th>Komentar</th>
											<th>Hapus</th>
										</tr>

	<?php
		
		$sql = "SELECT * FROM kontak WHERE status='1' ORDER BY id DESC";
		$cek = num_rows($sql);
		$arr = pager_isi($sql,10);
		
		if ($cek < 1) {
			echo "<tr><td colspan='2'><marquee>Data Kontak Masih Kosong..!!</marquee></td></tr>";

		} else {
			foreach ($arr as $baris) { 
				$id    = $baris['id'];
				$nama  = $baris['nama'];
				$mail  = $baris['email'];
				$komen = $baris['komen'];
				$jam   = $baris['jam'];
				$tgl   = $baris['tgl'];
				
				if ($baris[0]!='') {	
					$i++;
					echo "<tr>";
					echo "<td><div class='left_bg3'>
						<a href='mailto:$mail' class='user'>".ucwords($nama)."</a>&nbsp;&nbsp;&nbsp;
						<a class='time'>".convert_tanggal($tgl)." - $jam</a><br />
						".ucwords(nl2br($komen))."</div></td>";
					echo "<td>
						<a href='?act=Kontak.Lihat&Del=$id' class='btn btn-sm btn-danger' title='Hapus Komentar'
						onclick='return confirm(\"Yakin data akan dihapus..?\")'>Hapus</a>
						</td>";
					echo "</tr>";
				} 
			} 
		}
	?>

					</table>
				<div class="left_bg2"><?php $path = "?act=Kontak.Lihat";pager($sql,10,$path); ?></div>
					<div class="left_bg1"><a href='?act=Kontak.Lihat&Delete' class='btn btn-danger' title='Hapus Semua Komentar'
					onclick='return confirm("Yakin semua komentar akan dihapus..?")'>Hapus Seluruh Komentar</a></div>
			
					<?php
						$jum = num_rows("SELECT nama FROM kontak WHERE status='0'");
						if ($jum > 0) $link = "<a href='?act=Kontak.Komen' title='Komentar Baru' class='btn'>Komentar Baru ($jum)</a>";
						else $link = "<a title='Komentar Baru' class='btn btn-info'>Komentar Baru ($jum)</a>";

					?>
					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<ul class="pagination pull-right">
					<li><?=$link?></li>
				</ul>
			</div>
			<div class="clearfix"></div>

		</div>
	</div>
</div>
<?php require_once foot ?>