<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	if (isset($_POST['add_jarak'])) {
		$jarak  = valid_form($_POST['jarak']);
		$ongkos = valid_form($_POST['ongkos']); 

		if (!empty($jarak) && !empty($ongkos)) {
			$sql = "INSERT INTO jarak (tujuan,ongkos) VALUES ('$jarak','$ongkos')";
			$qry = query($sql);
			pesan_submit("?act=Jarak.Lihat");
			exit;
		}	

	}
	
	require_once head;
	
?>

<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Tambah Data Jarak</h3>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<br />
						<form action="" class="form-horizontal form-label-left" method="post" name="form_add_jarak" onsubmit='return cek_add_jarak()'>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Jarak Tujuan</label>
								<div class="col-md-5 col-sm-10 col-xs-12">
									<input name="jarak" type="text" class="form-control" maxlength="40" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Ongkos Tujuan</label>
								<div class="col-md-5 col-sm-10 col-xs-12">
									<input name="ongkos" type="text" class="form-control" maxlength="40" />
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-2">
									<input type="submit" name="add_jarak" class="btn btn-primary" value="Simpan" />
									<input type="reset" name="reset" class="btn btn-danger" value="Batal">
								</div>
							</div>
						</form>
                  	</div>
                </div>
          	</div>
        </div>
  	</div>
</div>
<?php require_once foot ?>