<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');

	if ($_GET['sts']==='delete') {
		$sql = "DELETE FROM kirim WHERE id_kirim='".$_GET['kode']."'";
		$qry = query($sql);
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ?act=Pesan.Kirim'>";
	}
	
	if (isset($_POST['pay'])) {
		$id_bayar = valid_form($_POST['id_bayar']);
		$id_pesan = valid_form($_POST['id_pesan']);
		$tgl = $_POST['tgl'];
		$bln = $_POST['bln'];
		$thn = $_POST['thn'];
		$tanggal = $thn."-".$bln."-".$tgl;

		if (empty($id_bayar) || empty($id_pesan)) {
			pesan_error("Data masih ada yang kosong");
			exit;
		
		} else {
			query("UPDATE bayar SET sts_kirim='sudah' WHERE id_bayar='$id_bayar'");
			$sql = "INSERT INTO kirim VALUES ('','$id_bayar','$tanggal')"; 
			query ($sql);
			header ("Location: ?act=Pesan.Kirim");

		}
	}

	require_once head;
	
?>
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Halaman Pengiriman</h3>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<br />
						<?php
						if (($_GET['sts']==='input')) {
							$status = "input";
							$pesan  = $_GET['pesan'];
							$bayar  = $_GET['bayar'];
						?>
						
						<form method='post' class="form-horizontal form-label-left" name='form_bayar' action=''>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Kode Bayar</label>
								<div class="col-md-5 col-sm-10 col-xs-12">
									<input type="text" name="id_bayar" class="form-control" value="<?=$bayar?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Kode Pesan</label>
								<div class="col-md-5 col-sm-10 col-xs-12">
									<input type="text" name="id_pesan" class="form-control" value="<?=$pesan?>">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Tanggal Kirim</label>
								<div class="col-md-2">
									<select name='tgl' class="form-control"><?php echo combo_tgl(); ?></select>
								</div>
								<div class="col-md-2">
									<select name='bln' class="form-control"><?php echo combo_bln(); ?></select>
								</div>
								<div class="col-md-2">
									<select name='thn' class="form-control"><?php echo combo_thn(); ?></select>
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-2">
									<input type="submit" name="pay" class="btn btn-primary" value="Simpan" />
								</div>
							</div>
						</form>
		
		<?php 
		
			} else {
				echo "<table class=\"table table-striped\">";
				echo "<tr><th>No</th><th>Id Kirim</th><th>Id Bayar</th><th>Id Pesan</th>
				<th>Nama User</th><th>Tanggal Bayar</th><th>Action</th></tr>";
				
				$sql = "SELECT k.id_kirim, b.id_bayar, p.id_pesan, u.nama, k.tgl_kirim ".
					   "FROM kirim AS k, bayar AS b, pesan AS p, user AS u ".
					   "WHERE k.id_bayar=b.id_bayar AND b.id_pesan=p.id_pesan AND p.id_user=u.id_user ORDER BY id_kirim DESC ";
								
				if (num_rows($sql) < 1) {
					echo "<tr><td colspan='7' align='center'><marquee>Data Masih Kosong</marquee></td></tr>";
				
				} else {
					$arr = pager_isi($sql,10);
					$i=no_baris();
					foreach ($arr as $baris) { 
						if ($baris[0]!='') {	
							$i++;
							echo "<tr>";
							echo "<td>$i</td>";
							echo "<td>$baris[0]</td>";
							echo "<td>$baris[1]</td>";
							echo "<td>$baris[2]</td>";
							echo "<td>".ucwords($baris[3])."</td>";
							echo "<td>".convert_tanggal($baris[4])."</td>";
							echo "<td><a href='?act=Pesan.Kirim&sts=delete&kode=$baris[0]'
								onclick=\"return confirm('Yakin data akan dihapus');\" title='Hapus Data' class='btn btn-sm btn-danger'>Hapus</a></td>";
							echo "</tr>";
						}
					} 
				}
		?>

					</table>
					
					<div class="left_bg2">
						<?php $path = "?act=Pesan.Kirim";pager($sql,10,$path); ?>
					</div>
					<?php } ?>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php require_once foot ?>