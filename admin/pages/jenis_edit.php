<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	if (isset($_POST['edit_jenis'])) {
		$kode  = $_POST['kode'];
		$jenis = valid_form($_POST['jenis']);
		
		if (!empty($jenis)) {
			$sql = "UPDATE jenis SET jenis='$jenis' WHERE id_jenis='$kode'";
			$qry = query($sql);
			pesan_submit("?act=Jenis.Lihat");
			exit;
		}	
	}
	
	require_once head;
	
?>

	<!-- start konten tengah-->
	<div class="center_content">
		
		<!-- start konten kiri-->
		<div class="left_content">
			<div class="left_box">
				<h3>Edit Jenis</h3>
				<?php 
					$kde = $_GET['Id'];
					$sql = "SELECT * FROM jenis WHERE id_jenis='$kde'";
					$qry = query($sql);
					list($kode,$jnis) = mysql_fetch_array($qry);
				?>
				<div class="admin_form">
					<form action="" method="post" name="form_edit_jenis" onsubmit='return cek_edit_jenis()'>
					<input type="hidden" name="kode" value="<?=$kode?>">
					<div class="row">
						<label class="form_admin"><strong>Jenis Produk :</strong></label>
						<input name="jenis" type="text" class="form_admin" maxlength="40" value="<?=$jnis?>"/>
					</div>
					<div class="row">
					<div style="margin:5px 0 0 205px;">
						<input type="submit" name="edit_jenis" class="submit" value="Simpan" />
						<input type="reset" name="reset" class="submit" value="Batal" />
					</div>
					</div>
					</form>
				</div>

			</div>
		</div>
		<!--end konten kiri-->
		
		<!-- start konten kanan-->
		<div class="right_content">
			<div class="right_box">
				
				<h3>Control Panel</h3>
				<div class="box_right_list">
				<ul class="right_list">
					<li><a href="?act=Jenis.Lihat" title="Daftar Jenis" class="news">Daftar Jenis</a></li>
				</ul>
				</div>

			</div>
		</div>
		<!--end konten kanan-->
	   
		<div class="clear"></div>
	
	</div>
	<!--end konten tengah-->

<?php require_once foot ?>