<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');

	if (isset($_POST['simpan_confirm'])) {
		$kode = $_POST['kode'];
		$kde  = $_POST['id_pesan'];
		$ket  = valid_form($_POST['keterangan']);
		$sql1 = "UPDATE konfirmasi SET keterangan='$ket', sts_baca='1' WHERE id_konf='$kode'";
		query($sql1);
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ?act=Pesan.Konfirmasi&stsBaca=1'>";

	} elseif ($_GET['sts']==='delete') {
		$sql = "DELETE FROM konfirmasi WHERE id_konf='".$_GET['kode']."'";
		$qry = query($sql);
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ?act=Pesan.Konfirmasi'>";
	}

	require_once head;
	
?>
<div class="right_col" role="main">
	<div class="">
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
		
<?php

	if ($_GET['sts']==='Baca') {
		$sql = "SELECT b.id_konf, b.id_pesan, p.id_user, b.tgl, b.no_bukti, p.ttl_bayar, b.jum_bayar, b.keterangan ".
			   "FROM konfirmasi AS b, pesan AS p ".
			   "WHERE p.id_pesan=b.id_pesan AND b.id_konf='".$_GET['kode']."'";			   
		$isi = query($sql);
		$row = mysql_fetch_array($isi);

		# cek jika konsumen sudah melakukan konfirmasi pembayaran 

		$sql = "SELECT jum_bayar FROM konfirmasi ".
			   "WHERE id_pesan = '".$row['id_pesan']."' AND sts_baca='1'";
		$isi = query($sql);
		if (mysql_num_rows($isi) > 0) {
			echo "<h3>Daftar konfirmasi pembayaran sebelumnya.</h3>";
			echo "<table class='style1' width='200px'>";
			echo "<tr><th>No.</th><th>Jumlah Bayar</th></tr>";
			$i=0;
			$total=0;
			while ($brs = mysql_fetch_row($isi)) {
				$i++;
				echo "<tr>";
				echo "<td align='right' width='20px'>$i</td>";
				echo "<td align='right'>".format_uang($brs[0])."</td>";
				echo "</tr>";
				$total += $brs[0];
			}			
			echo "<tr><th colspan='2' class='sumery'><div align='right'>".format_uang($total)."</div></th></tr>";
			echo "</table><hr/>";
		}

?>
		<h3>Konfirmasi Bayar</h3>
		<div class="left_bg1">
		<form action="" method="post" name="form_konfirmasi">
		<input name="kode" type="hidden" value="<?=$row['id_konf']?>" />
			<div class="row">
				<label class="nota3"><strong>Kode Pesan : </strong></label>
				<?=$row['id_pesan']?>
			</div>
			<div class="row">
				<label class="nota3"><strong>Nama Konsumen : </strong></label>
				<?=ucwords(nama_konsumen($row['id_user']))?>
			</div>
			<div class="row">
				<label class="nota3"><strong>Tanggal Bayar : </strong></label>
				<?=convert_tanggal($row['tgl'])?>
			</div>
			<div class="row">
				<label class="nota3"><strong>No Bukti : </strong></label>
				<?=$row['no_bukti']?>
			</div>
			<div class="row">
				<label class="nota3"><strong>Total Belanja : </strong></label>
				<?=format_uang($row['ttl_bayar'])?>
			</div>
			<div class="row">
				<label class="nota3"><strong>Total Bayar : </strong></label>
				<?=format_uang($row['jum_bayar'])?>
			</div>
			<div class="row">
				<label class="nota3"><strong>Keterangan : </strong></label>
				<textarea name="keterangan" class="form_admin"><?=$row['ket']?></textarea>
			</div>
			<div class="row"><div style="margin:0 0 0 125px;">
			<input type="submit" name="simpan_confirm" value="Simpan" class="submit" 
				onClick="return confirm('Yakin akan disimpan.?');"/>&nbsp;
			<input type="button" name="batal" value="Batal" class="submit"
				onClick="window.history.back();"/>
			</div></div>
		</form>
		</div>

	<?php
			
		} else {

			if (isset($_GET['stsBaca'])) 
				echo "<h3>Daftar Konfirmasi Pembayaran Sudah Dibaca</h3>";
			else
				echo "<h3>Daftar Konfirmasi Pembayaran Belum Dibaca</h3>";
		
			echo "<table class=\"table table-striped\">";
			echo "<tr><th>No</th><th>Kd Pesan</th><th>Nama User</th><th>Tanggal Bayar</th>
				<th>No Bukti</th><th>Jumlah Bayar</th><th>Action</th></tr>";

			if (isset($_GET['stsBaca'])) {
				$sql = "SELECT b.id_pesan, p.id_user, b.tgl, b.no_bukti, p.ttl_bayar, b.jum_bayar, b.id_konf ".
					   "FROM konfirmasi AS b, pesan AS p ".
					   "WHERE p.id_pesan=b.id_pesan AND b.sts_baca='1' ORDER BY b.id_pesan, b.id_konf DESC ";
			} else {
				$sql = "SELECT b.id_pesan, p.id_user, b.tgl, b.no_bukti, p.ttl_bayar, b.jum_bayar, b.id_konf ".
					   "FROM konfirmasi AS b, pesan AS p ".
					   "WHERE p.id_pesan=b.id_pesan AND b.sts_baca='0' ORDER BY b.id_pesan, b.id_konf DESC ";
			}
			
			if (num_rows($sql) < 1) {
				echo "<tr><td colspan='8' align='center'><marquee>Data Masih Kosong</marquee></td></tr>";
			
			} else {
				$arr = pager_isi($sql,10);
				$i=no_baris();
				foreach ($arr as $baris) { 
					if ($baris[0]!='') {	
						$i++;
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$baris[0]</td>";
						echo "<td>".ucwords(nama_konsumen($baris[1]))."</td>";
						echo "<td>".convert_tanggal($baris[2])."</td>";
						echo "<td>$baris[3]</td>";
						echo "<td>".format_uang($baris[5])."</td>";
						if (isset($_GET['stsBaca'])) {
							echo "<td>".
							"<a href='?act=Pesan.Bayar&sts=input&pesan=$baris[0]&bukti=$baris[3]&bayar=$baris[5]' class='btn btn-sm btn-info'>Edit</a>";
						} else {
							echo "<td><a href='?act=Pesan.Konfirmasi&sts=Baca&kode=$baris[6]' class='btn btn-sm btn-info'>Edit</a>";
						}
						echo "<a href='?act=Pesan.Konfirmasi&sts=delete&kode=$baris[6]'
							onclick=\"return confirm('Yakin data akan dihapus');\" title='Hapus Data' class='btn btn-sm btn-danger'>Hapus</a></td>";
						echo "</tr>";
					}
				} 
			}
		?>
		</table>
		
		<div class="left_bg2">
			<?php $path = "?act=Pesan.Konfirmasi";pager($sql,10,$path); ?>
		</div>

		<div class="left_bg2">
			<a href='?act=Pesan.Konfirmasi&stsBaca=1' class="btn">Konfirmasi Sudan Dibaca</a>&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="?act=Pesan.Konfirmasi" class="btn">Konfirmasi Belum Dibaca</a>
		</div>

	<?php } ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php require_once foot ?>