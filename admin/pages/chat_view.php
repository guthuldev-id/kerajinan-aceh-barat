<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	if (isset($_GET['Del'])) {
		$id  = $_GET['Del'];
		$sql = "DELETE FROM chatbox WHERE id='$id'";
		$qry = query($sql);
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ?act=Chat.Lihat'>";
		exit;
	}
	
	if (isset($_GET['Delete'])) {
		$sql = "DELETE FROM chatbox";
		$qry = query($sql);
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ?act=Chat.Lihat'>";
		exit;
	}

	require_once head;
	
?>

	<!-- start konten tengah-->
	<div class="center_content">
		
		<!-- start konten kiri-->
		<div class="left_content">
			<div class="left_box">
				<h3>Daftar Data Chatbox</h3>
				<table class="style1" width="98%">
					<tr><th width="480px">Chat</th><th width="30px">Hapus</th></tr>

	<?php
		
		$sql = "SELECT * FROM chatbox ORDER BY id DESC";
		$cek = num_rows($sql);
		$arr = pager_isi($sql,10);
		
		if ($cek < 1) {
			echo "<tr><td colspan='2'><marquee>Data Chatbox Masih Kosong..!!</marquee></td></tr>";

		} else {
			foreach ($arr as $baris) { 
				$kode = $baris['id'];
				$nama = $baris['nama'];
				$mail = $baris['email'];
				$chat = $baris['chat'];
				$tgl  = $baris['tgl'];
				$jam  = $baris['jam'];
				
				if ($baris[0]!='') {	
					$i++;
					echo "<tr>";
					echo "<td><div class='left_bg3'>
						<a href='mailto:$mail' class='user'>$nama</a>&nbsp;
						<a class='time'>".convert_tanggal($tgl)." - $jam</a><br />
						".ucfirst(nl2br($chat))."<br />
						</div></td>";
					echo "<td valign='top' align='center'>
						<a href='?act=Chat.Lihat&Del=$kode' class='delete' title='Hapus Chatbox'
						onclick='return confirm(\"Yakin data akan dihapus..?\")'></a>
						</td>";
					echo "</tr>";
				} 
			} 
		}
	?>

				</table>
				<div class="left_bg2"><?php $path = "?act=Chat.Lihat";pager($sql,10,$path); ?></div>
				<div class="left_bg1"><a href='?act=Chat.Lihat&Delete' class='delete' title='Hapus Semua Chatbox'
				onclick='return confirm("Yakin semua komentar akan dihapus..?")'>Hapus Seluruh Chatbox</a></div>
			</div>
		</div>
		<!--end konten kiri-->

		<!-- start konten kanan-->
		<div class="right_content">
			<div class="right_box">
				
				<h3>Control Panel</h3>
				<div class="box_right_list">
				<ul class="right_list">
					<li><a href="?act=Faq.Lihat" class="news">Olah Data Faq</a></li>
				</ul>
				</div>

			</div>
		</div>
		<!--end konten kanan-->
	   
		<div class="clear"></div>
	
	</div>
	<!--end konten tengah-->

<?php require_once foot ?>