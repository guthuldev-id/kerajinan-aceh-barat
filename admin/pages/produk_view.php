<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	function ambil_gambar($kdProduk) {
		return fetch_row("SELECT gambar FROM produk WHERE id_produk='$kdProduk'");
	}

	function hapus_gambar($kdProduk) {
		$gbr = ambil_gambar($kdProduk);
		if ($gbr!='') {
			$fileGmbr = "../".$gbr;
			if (file_exists($fileGmbr)) unlink($fileGmbr);
		}
	}
	
	if (isset($_GET['Del'])) {
		$id  = $_GET['Del'];
		hapus_gambar($id);
		$sql = "DELETE FROM produk WHERE id_produk='$id'";
		$qry = query($sql);
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ?act=Produk.Lihat'>";
		exit;
	}
	
	require_once head;
	
?>
	<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Manajemen <small>Data Produk</small></h3>
			</div>

			<div class="title_right">
				<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search for...">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button">
								Go!
							</button> </span>
					</div>
				</div>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">

			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<a href="?act=Produk.Tambah" title="Tambah Produk" class="btn btn-info">Tambah Produk</a>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
					<!-- start konten tengah-->
	<div class="center_content">
		
		<!-- start konten kiri-->
		<div class="left_content">
			<div class="left_box">
				<h3>Daftar Produk</h3>
				<table class="table table-striped">
					<tr>
						<th align="right" width="20">No</th>
						<th width="100px">Jenis</th>
						<th width="150px">Produk</th>
						<th width="100px">Harga</th>
						<th width="50px">Berat</th>
						<th width="30px">Action</th>
					</tr>

	<?php
		
		$sql = "SELECT p.*, j.* FROM produk AS p, jenis AS j WHERE p.id_jenis=j.id_jenis ORDER BY id_produk DESC";
		$cek = num_rows($sql);
		$arr = pager_isi($sql,10);
		$i = no_baris(10);
		
		if ($cek < 1) {
			echo "<tr><td colspan='6'><marquee>Data Produk Masih Kosong..!!</marquee></td></tr>";

		} else {
			foreach ($arr as $baris) { 
				$id     = $baris['id_produk'];
				$produk = $baris['produk'];
				$jenis  = $baris['jenis'];
				$harga  = $baris['harga'];
				$berat  = $baris['berat'];

				if ($baris[0]!='') {	
					$i++;
					echo "<tr>";
					echo "<td align='right'>$i</td>";
					echo "<td>".ucwords($jenis)."</td>";
					echo "<td>".ucwords($produk)."</td>";
					echo "<td>".format_uang($harga)."</td>";
					echo "<td>".$berat."Kg</td>";
					echo "<td valign='top'>
						<a href='?act=Produk.Detail&Id=$id' class='detail' title='Detail Produk'>Detail</a>
						<a href='?act=Produk.Edit&Id=$id' class='edit' title='Edit Produk'>Edit</a>
						<a href='?act=Produk.Lihat&Del=$id' class='delete' title='Hapus Produk' onclick='return confirm(\"Yakin data akan dihapus..?\")'>Hapus</a>
						</td>";
					echo "</tr>";
				} 
			} 
		}
	?>

				</table>

					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<ul class="pagination pull-right">
				<?php $path = "?act=Produk.Lihat";pager($sql,10,$path); ?>
			</div>
			<div class="clearfix"></div>

		</div>
	</div>
</div>
<!-- /page content -->

<?php require_once foot ?>