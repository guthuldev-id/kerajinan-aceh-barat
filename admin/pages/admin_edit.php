<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	if (isset($_POST['edit_admin'])) {
		$user  = valid_form($_POST['user']);
		$pass  = valid_form($_POST['pass']);
		$pass1 = valid_form($_POST['pass1']);
		$pass2 = valid_form($_POST['pass2']);
		
		if (!cek_pass_admin($user, $pass)) {
			pesan_error("Password lama tidak sama");
		
		} else if (strlen($pass1) < 6) {
			pesan_error("Minimal panjang karakter password adalah 6 karakter");
			
		} else if ($pass1 != $pass2) {
			pesan_error("Password pertama dan kedua anda tidak sama");
				
		} else if (!empty($user) && !empty($pass) && !empty($pass1) && !empty($pass2)) {
			$sql = "UPDATE admin SET pass=md5('$pass1') WHERE user='$user'";
			$qry = query($sql);
			pesan_submit("?act=Admin.Lihat");
			exit;

		}
	}
	
	require_once head;
	
?>

	<!-- start konten tengah-->
	<div class="center_content">
		
		<!-- start konten kiri-->
		<div class="left_content">
			<div class="left_box">
				<h3>Ganti Password Administrator</h3>
				<div class="admin_form">
					<form action="" method="post" name="form_edit_admin" onsubmit='return cek_edit_admin()'>
					<div class="row">
						<label class="form_admin"><strong>Username :</strong></label>
						<input name="user" type="text" class="form_admin" maxlength="40" value="<?=$_SESSION['ADMIN_RONAL']?>"/>
					</div>  
					<div class="row">
						<label class="form_admin"><strong>Password Lama :</strong></label>
						<input name="pass" type="password" class="form_admin" maxlength="40" />
					</div>
					<div class="row">
						<label class="form_admin"><strong>Password (Minimal 6 Karakter) :</strong></label>
						<input name="pass1" type="password" class="form_admin" maxlength="40" />
					</div>
					<div class="row">
						<label class="form_admin"><strong>Konfirmasi Password :</strong></label>
						<input name="pass2" type="password" class="form_admin" maxlength="40" />
					</div>
					<div class="row">
					<div style="margin:5px 0 0 55px;">
						<input type="submit" name="edit_admin" class="submit" value="Simpan" />
						<input type="reset" name="reset" class="submit" value="Batal" />
					</div>
					</div>
					</form>
				</div>
			</div>
		</div>
		<!--end konten kiri-->
		
		<!-- start konten kanan-->
		<div class="right_content">
			<div class="right_box">
				
				<h3>Control Panel</h3>
				<div class="box_right_list">
				<ul class="right_list">
					<li><a href="?act=Admin.Lihat" title="Daftar Administrator" class="user">Daftar Administrator</a></li>
					<li><a href="?act=Admin.Tambah" title="Tambah Administrator" class="user">Tambah Administrator</a></li>
				</ul>
				</div>

			</div>
		</div>
		<!--end konten kanan-->
	   
		<div class="clear"></div>
	
	</div>
	<!--end konten tengah-->

<?php require_once foot ?>