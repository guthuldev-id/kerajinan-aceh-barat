<?php 

	defined('RONAL') or die('<b>Cannot Access..!!</b>');
	
	$id  = $_GET['Id'];
	$sql = "SELECT * FROM user WHERE id_user='$id'";
	$qry = query($sql);
	list($id,$nama,$user,$pass,$almt,$kota,$prop,$sex,$kdpos,$tlp,$mail) = mysql_fetch_array($qry);
	$sex = cek_sex($sex);
	
	require_once head;
	
?>

	<!-- start konten tengah-->
	<div class="center_content">
		
		<!-- start konten kiri-->
		<div class="left_content">
			<div class="left_box">
				<h3>Detail User</h3>
				<div class="admin_form">
					<div class="row">
						<label class="det_admin1"><strong>Nama User :</strong></label>
						<label class="det_admin2"><?=ucwords($nama)?></label>
					</div>
					<div class="row">
						<label class="det_admin1"><strong>Username :</strong></label>
						<label class="det_admin2"><?=$user?></label>
					</div>
					<div class="row">
						<label class="det_admin1"><strong>Jenis Kelamin :</strong></label>
						<label class="det_admin2"><?=$sex?></label>
					</div>
					<div class="row">
						<label class="det_admin1"><strong>Alamat :</strong></label>
						<label class="det_admin2"><?=ucwords($almt)?></label>
					</div>
					<div class="row">
						<label class="det_admin1"><strong>Kota :</strong></label>
						<label class="det_admin2"><?=ucwords($kota)?></label>
					</div>
					<div class="row">
						<label class="det_admin1"><strong>Propinsi :</strong></label>
						<label class="det_admin2"><?=$prop?></label>
					</div>
					<div class="row">
						<label class="det_admin1"><strong>Kode Pos :</strong></label>
						<label class="det_admin2"><?=$kdpos?></label>
					</div>
					<div class="row">
						<label class="det_admin1"><strong>No Telepon :</strong></label>
						<label class="det_admin2"><?=$tlp?></label>
					</div>
					<div class="row">
						<label class="det_admin1"><strong>Email :</strong></label>
						<label class="det_admin2"><?=$mail?></label>
					</div>
					<br /><br />
				</div>
					<p><a href="?act=User.Lihat" class='user'>Daftar User</a> 
					<a href="?act=User.Tambah" class='user'>Tambah User</a></p>
			</div>
		</div>
		<!--end konten kiri-->
		
		<!-- start konten kanan-->
		<div class="right_content">
			<div class="right_box">
				
				<h3>Control Panel</h3>
				<div class="box_right_list">
				<ul class="right_list">
					<li><a href="?act=User.Lihat" title="Daftar User" class="user">Daftar User</a></li>
					<li><a href="?act=User.Tambah" title="Tambah User" class="user">Tambah User</a></li>
				</ul>
				</div>

			</div>
		</div>
		<!--end konten kanan-->
	   
		<div class="clear"></div>
	
	</div>
	<!--end konten tengah-->

<?php require_once foot ?>