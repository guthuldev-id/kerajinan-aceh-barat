<?php 
	
	define('RONAL',TRUE);
	require_once '../setting.php';
	require_once '../'.fungsi;
	
	if (!cek_sessi_admin()) {
		echo "<META HTTP-EQUIV = 'Refresh' Content = '0; URL = ./'>";
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
	<title>Laporan | Sekido Shop</title>
</head>

<style>

body {
	font-family:Arial, Helvetica, sans-serif;
	padding:0;
	font-size:12px;
	margin:0px auto auto auto;
	color:#000000;
}

p {
	margin:5px 7px 5px 7px;
	text-align:justify;
	line-height:20px;
}

a {
	color:#b04c26;
	text-decoration:none;
}

a:hover {
	color:#000;
	cursor:pointer;
}

table {
	margin:0 2px 0 7px;
	border-collapse: collapse;
	border-spacing:0;
	border:1px solid #e8e7e1;
	background:none;
}

table td, th{
	padding:5px;
	border:1px solid #e8e7e1;
} 

table th{
	padding:5px;
	background:#f7f6f0;
	text-align:left;
}

.left_content {
	width:600px;
	float:left;
	margin:10px 0 10px 10px;
	padding:10px;
	background:#fff;
}

.left_box {
	width:595px;
	border:1px solid #e8e7e1;
	min-height:150px;
	margin:0 2px 10px 2px;
}

.left_box h3 {
	width:585px;
	border-bottom:1px solid #e8e7e1;
	background:#f7f6f0;
	padding:5px;
	margin:0 0 5px 0;
}

.left_bg {
	width:573px;
	border:1px solid #e8e7e1;
	margin:5px;
	padding:5px;
}

.row {
	width:550px;
	clear:both;
	padding:5px 0 5px 0;
}

label.nota3 {
	width:120px;
	float:left;
	font-size:12px;
	text-align:right;
	padding:0 5px 0 0;
	color: #333333;
}

.close {
	padding:0 0 0 20px;
	background:transparent url(../images/ico_logout.png) no-repeat left;
	font-weight:bold;
}

.print {
	padding:0 0 0 20px;
	background:transparent url(../images/ico_print.png) no-repeat left;
	font-weight:bold;
}

select.tgl {
	width:50px;
	height:24px;
	margin:0 2px 0 0;
	padding:2px 0 0 0;
	background-color:#fff;
	color:#999999;
	border:1px #DFDFDF solid;
	float:left;
}

select.bln {
	width:50px;
	height:24px;
	margin:0 2px 0 0;
	padding:2px 0 0 0;
	background-color:#fff;
	color:#999999;
	border:1px #DFDFDF solid;
	float:left;
}

select.thn {
	width:60px;
	height:24px;
	margin:0 2px 0 0;
	padding:2px 0 0 0;
	background-color:#fff;
	color:#999999;
	border:1px #DFDFDF solid;
	float:left;
}

input.submit {
	width:71px;
	height:25px;
	border:none;
	cursor:pointer;
	text-align:center;
	border:1px #DFDFDF solid;
	color:#000;
}

</style>

<script type="text/javascript">

	function print_page() {
		if (typeof(window.print) != 'undefined') {
			window.print();
		}
	}

</script>

<body>

	<div class="left_content">
		<div class="left_box">											
			
			<?php
			
			if (isset($_GET['sts']) && $_GET['sts'] == 'Detail') {
				$id = $_GET['Id'];
				$sql = "SELECT id_pesan, id_jarak FROM pesan WHERE id_pesan='".$id."'";
				list($kd_pesan,$kd_jasa) = mysql_fetch_row(query($sql));
				$tarif = tarif_jarak($kd_jasa);

				$sql = "SELECT u.nama, p.tujuan, p.kota, p.propinsi, p.kd_pos, p.telepon, u.email ".
					   "FROM user AS u, pesan AS p ".
					   "WHERE p.id_user=u.id_user AND p.id_pesan='$kd_pesan'";
				$row = mysql_fetch_row(query($sql));

				echo "<p></p><table width='98%'>";
				echo "<tr><th colspan='6'>Detail Pesan : $id</th></tr>";
				echo "<tr><th>No</th><th>Nama Produk</th><th>Harga</th><th>Berat</th><th>Qty</th><th>Total</th></tr>";

				$sql = "SELECT id_pesan, id_jarak FROM pesan WHERE id_pesan='".$kd_pesan."'";
				list($kd_pesan,$kd_jasa) = mysql_fetch_row(query($sql));
				$tarif = tarif_jarak($kd_jasa);
					
				$sql = "SELECT p.produk, d.jumlah_barang, d.harga_satuan, (p.berat * d.jumlah_barang) AS sberat, ".
					   "(d.jumlah_barang * d.harga_satuan) AS stotal ".
					   "FROM produk AS p, pesan_detail AS d ".
					   "WHERE d.id_produk=p.id_produk AND d.id_pesan='$kd_pesan'";
				$isi = query($sql);
				$no=0;
				while (list($nama,$jum,$harga,$sberat,$stotal) = mysql_fetch_row($isi))	{	
					$no++;		
					$total += $stotal;
					$berat += $sberat;

					echo "<tr $no>";
					echo "<td align='center'>".$no."</td>";
					echo "<td align='left'>".ucwords($nama)."</td>";
					echo "<td align='right'>".format_uang($harga)."</td>";
					echo "<td align='center'>".$sberat."Kg</td>";
					echo "<td align='center'>".$jum."</td>";
					echo "<td align='right'>".format_uang($stotal)."</td>";
					echo "</tr>";
				} 
				
				if ($no>0) {
					$sberat = ceil($berat);
					$starif = $tarif*$sberat;
					$skabeh = $total+$starif;
					echo "<tr><td colspan='6' align='right'>
						<strong>Total Belanja Anda : </strong>".format_uang($total)."</td></tr>";
					echo "<tr><td colspan='6' align='right'>
						<strong>Ongkos Kirim Per Kg : </strong>".format_uang($tarif)."</td></tr>";
					echo "<tr><td colspan='6' align='right'>
						<strong>Total Ongkos Kirim : </strong>".format_uang($starif)."</td></tr>";
					echo "<tr><td colspan='6' align='right'>
						<strong>Total Keseluruhan : </strong>".format_uang($skabeh)."</td></tr>";
				}
				
				echo "</table>";
				
				echo "<div class='left_bg'><strong>Terbilang : </strong>";
				echo terbilang($skabeh,3);
				echo "</div>";

				echo "<table class='style1' width='98%'>";
				echo "<tr><td width='120px' align='right'><strong>Atas Nama :</strong> 
					</td><td>".ucwords($row[0])."</td></tr>";
				echo "<tr><td width='120px' align='right'><strong>Alamat Tujuan :</strong> 
					</td><td>".ucwords($row[1])."</td></tr>";
				echo "<tr><td width='120px' align='right'><strong>Kota Tujuan :</strong> 
					</td><td>".ucwords($row[2])."</td></tr>";
				echo "<tr><td width='120px' align='right'><strong>Propinsi Tujuan :</strong> 
					</td><td>".ucwords($row[3])."</td></tr>";
				echo "<tr><td width='120px' align='right'><strong>Kode Pos Tujuan :</strong> 
					</td><td>".ucwords($row[4])."</td></tr>";
				echo "<tr><td width='120px' align='right'><strong>Telepon Pemesan :</strong> 
					</td><td>".ucwords($row[5])."</td></tr>";
				echo "<tr><td width='120px' align='right'><strong>Email Pemesan :</strong> 
					</td><td>$row[6]</td></tr>";
				echo "</table>";

				echo "<div class='left_bg'>";
				echo "<a href='#' class='print' onclick=\"javascript:print_page()\">Cetak</a>&nbsp;&nbsp;&nbsp;&nbsp;";
				echo "<a href='report.php' class='close'>Kembali</a>";
				echo "</div>";

	
			} else {
			
				if (isset($_GET['show']) && $_GET['show'] == 'sudah') {
					$tgl1 = $_POST['tgl1']; $bln1 = $_POST['bln1']; $thn1 = $_POST['thn1'];
					$tgl2 = $_POST['tgl2']; $bln2 = $_POST['bln2']; $thn2 = $_POST['thn2'];
					$tanggal1 = $thn1."-".$bln1."-".$tgl1;
					$tanggal2 = $thn2."-".$bln2."-".$tgl2;
					$where  =  "AND b.tgl >= '$tanggal1' AND b.tgl <= '$tanggal2' ";

					if (isset($_POST['all'])) {
						$sql = "SELECT b.id_konf, b.id_pesan, p.id_user, b.tgl, b.no_bukti, p.ttl_bayar, b.jum_bayar ".
							   "FROM konfirmasi AS b, pesan AS p ".
							   "WHERE p.id_pesan=b.id_pesan AND b.sts_baca='1' ORDER BY b.id_pesan, b.id_konf DESC ";
						$asd = "Laporan keseluruhan data konfirmasi sudah di baca";
					} else {
						$sql = "SELECT b.id_konf, b.id_pesan, p.id_user, b.tgl, b.no_bukti, p.ttl_bayar, b.jum_bayar ".
							   "FROM konfirmasi AS b, pesan AS p ".
							   "WHERE p.id_pesan=b.id_pesan AND b.sts_baca='1' $where ORDER BY b.id_pesan, b.id_konf DESC ";
						$asd = "Laporan data konfirmasi sudah di baca periode ".convert_tanggal($tanggal1)." s/d ".convert_tanggal($tanggal2);
					}
						
					echo "<p></p><table class='style1' width='98%'>";
					echo "<tr><th colspan='7'>$asd</th></tr>";
					echo "<tr><th>No</th><th>Kd Pesan</th><th>Nama User</th><th>Tanggal Bayar</th>
						 <th>No Bukti</th><th>Jumlah Bayar</th><th>Action</th></tr>";
						
						$pag = paging($sql,15);
						$arr = $pag->getPageData();
						$lin = $pag->getLinks();
						$i = no_baris();
						if ($pag->numItems()<=0) {
							echo "<tr><td colspan='7'>Data Masih Kosong..!!</td></tr>";
						} else {
							foreach ($arr as $baris) { 
								$i++;
								$id_konf = $baris[0]; 
								$id_psan = $baris[1]; 
								$id_user = $baris[2];
								$tgl = $baris[3]; 
								$bkt = $baris[4]; 
								$ttl = $baris[5]; 
								$jum = $baris[6];
								echo "<tr>";
								echo "<td width='20' align='right'>$i&nbsp;</td>";
								echo "<td width='60'>".$id_psan."</td>";
								echo "<td align='left'>".nama_konsumen($id_user)."</td>";
								echo "<td align='center'>".convert_tanggal($tgl)."</td>";
								echo "<td align='center'>$bkt</td>";
								echo "<td align='right'>".format_uang($jum)."</td>";
								echo "<td align='center' width='10px'><a href='?sts=Detail&Id=$id_psan' class='print'></a>";
								echo "</tr>";
								
							} 
						}
					
					echo "</table>";				
					echo "<div class='left_bg' align='center'>".$lin['all']."</div>";
					echo "<p><a href='report.php' class='close'>Kembali</a></p>";
				
		
				} else if (isset($_GET['show']) && $_GET['show'] == 'belum') {
					$tgl1 = $_POST['tgl1']; $bln1 = $_POST['bln1']; $thn1 = $_POST['thn1'];
					$tgl2 = $_POST['tgl2']; $bln2 = $_POST['bln2']; $thn2 = $_POST['thn2'];
					$tanggal1 = $thn1."-".$bln1."-".$tgl1;
					$tanggal2 = $thn2."-".$bln2."-".$tgl2;
					$where  =  "AND b.tgl >= '$tanggal1' AND b.tgl <= '$tanggal2' ";

					if (isset($_POST['all'])) {
						$sql = "SELECT b.id_konf, b.id_pesan, p.id_user, b.tgl, b.no_bukti, p.ttl_bayar, b.jum_bayar ".
							   "FROM konfirmasi AS b, pesan AS p ".
							   "WHERE p.id_pesan=b.id_pesan AND b.sts_baca='0' ORDER BY b.id_pesan, b.id_konf DESC ";
						$asd = "Laporan keseluruhan data konfirmasi belum di baca";
					} else {
						$sql = "SELECT b.id_konf, b.id_pesan, p.id_user, b.tgl, b.no_bukti, p.ttl_bayar, b.jum_bayar ".
							   "FROM konfirmasi AS b, pesan AS p ".
							   "WHERE p.id_pesan=b.id_pesan AND b.sts_baca='0' $where ORDER BY b.id_pesan, b.id_konf DESC ";
						$asd = "Laporan data konfirmasi belum di baca periode ".convert_tanggal($tanggal1)." s/d ".convert_tanggal($tanggal2);
					}
						
					echo "<p></p><table class='style1' width='98%'>";
					echo "<tr><th colspan='7'>$asd</th></tr>";
					echo "<tr><th>No</th><th>Kd Pesan</th><th>Nama User</th><th>Tanggal Bayar</th>
						 <th>No Bukti</th><th>Jumlah Bayar</th><th>Action</th></tr>";
						
						$pag = paging($sql,15);
						$arr = $pag->getPageData();
						$lin = $pag->getLinks();
						$i = no_baris();
						if ($pag->numItems()<=0) {
							echo "<tr><td colspan='7'>Data Masih Kosong..!!</td></tr>";
						} else {
							foreach ($arr as $baris) { 
								$i++;
								$id_konf = $baris[0]; 
								$id_psan = $baris[1]; 
								$id_user = $baris[2];
								$tgl = $baris[3]; 
								$bkt = $baris[4]; 
								$ttl = $baris[5]; 
								$jum = $baris[6];
								echo "<tr>";
								echo "<td width='20' align='right'>$i&nbsp;</td>";
								echo "<td width='60'>".$id_psan."</td>";
								echo "<td align='left'>".nama_konsumen($id_user)."</td>";
								echo "<td align='center'>".convert_tanggal($tgl)."</td>";
								echo "<td align='center'>$bkt</td>";
								echo "<td align='right'>".format_uang($jum)."</td>";
								echo "<td align='center' width='10px'><a href='?sts=Detail&Id=$id_psan' class='print'></a>";
								echo "</tr>";
								
							} 
						}
					
					echo "</table>";				
					echo "<div class='left_bg' align='center'>".$lin['all']."</div>";
					echo "<p><a href='report.php' class='close'>Kembali</a></p>";
				

				} else if (isset($_GET['show']) && $_GET['show'] == 'kirim') {
					$tgl1 = $_POST['tgl1']; $bln1 = $_POST['bln1']; $thn1 = $_POST['thn1'];
					$tgl2 = $_POST['tgl2']; $bln2 = $_POST['bln2']; $thn2 = $_POST['thn2'];
					$tanggal1 = $thn1."-".$bln1."-".$tgl1;
					$tanggal2 = $thn2."-".$bln2."-".$tgl2;
					$where  =  "AND k.tgl_kirim >= '$tanggal1' AND k.tgl_kirim <= '$tanggal2' ";

					if (isset($_POST['all'])) {
						$sql = "SELECT b.id_bayar, b.id_pesan, p.id_user, k.id_kirim, b.jum_bayar, k.tgl_kirim
								FROM bayar AS b, pesan AS p, kirim AS k
								WHERE b.id_pesan=p.id_pesan AND k.id_bayar=b.id_bayar ORDER BY b.id_pesan DESC ";
						$asd = "Laporan keseluruhan data pengiriman";
					} else {
						$sql = "SELECT b.id_bayar, b.id_pesan, p.id_user, k.id_kirim, b.jum_bayar, k.tgl_kirim
								FROM bayar AS b, pesan AS p, kirim AS k
								WHERE b.id_pesan=p.id_pesan AND k.id_bayar=b.id_bayar $where ORDER BY b.id_pesan DESC ";
						$asd = "Laporan data pengiriman periode ".convert_tanggal($tanggal1)." s/d ".convert_tanggal($tanggal2);
					}
						
					echo "<p></p><table class='style1' width='98%'>";
					echo "<tr><th colspan='6'>$asd</th></tr>";
					echo "<tr><th>No</th><th>Kd Pesan</th><th>Nama User</th><th>Jumlah Bayar</th>
						 <th>Tanggal Kirim</th><th>Action</th></tr>";
						
						$pag = paging($sql,15);
						$arr = $pag->getPageData();
						$lin = $pag->getLinks();
						$i = no_baris();
						if ($pag->numItems()<=0) {
							echo "<tr><td colspan='6'>Data Masih Kosong..!!</td></tr>";
						} else {
							foreach ($arr as $baris) { 
								$i++;
								$id_byr = $baris[0]; 
								$id_psn = $baris[1]; 
								$id_usr = $baris[2];
								$id_krm = $baris[3];
								$jum = $baris[4];
								$tgl = $baris[5]; 								
								echo "<tr>";
								echo "<td width='20' align='right'>$i&nbsp;</td>";
								echo "<td width='60'>".$id_psn."</td>";
								echo "<td align='left'>".nama_konsumen($id_usr)."</td>";
								echo "<td align='center'>".format_uang($jum)."</td>";
								echo "<td align='right'>".convert_tanggal($tgl)."</td>";
								echo "<td align='center' width='10px'><a href='?sts=Detail&Id=$id_psn' class='print'></a>";
								echo "</tr>";
								
							} 
						}
					
					echo "</table>";				
					echo "<div class='left_bg' align='center'>".$lin['all']."</div>";
					echo "<p><a href='report.php' class='close'>Kembali</a></p>";


				} else {

				?>
				
				<h3>Menu Laporan</h3>
					
					<table width='98%'>
						<form name='konf_udah' method='post' action='?show=sudah'>
						<tr><th colspan='4'>Laporan Konfirmasi Sudah Dibaca</th></tr>
						<tr>
							<td align='right' width='80px'>Keseluruhan : </td><td colspan='2'></td>
							<td align='center'><input type='submit' name='all' value='Tampil' class='submit'></td>
						</tr>
						<tr>
							<td align='right' width='80px'>Per Periode :</td>
							<td width='190px'>
								<select name='tgl1' class="tgl"><?php echo combo_tgl_lap(); ?></select>
								<select name='bln1' class="bln"><?php echo combo_bln_lap(); ?></select>
								<select name='thn1' class="thn"><?php echo combo_thn_lap(); ?></select>
							</td>
							<td width='190px'>
								<select name='tgl2' class="tgl"><?php echo combo_tgl_lap(); ?></select>
								<select name='bln2' class="bln"><?php echo combo_bln_lap(); ?></select>
								<select name='thn2' class="thn"><?php echo combo_thn_lap(); ?></select>
							</td>
							<td align='center'>
								<input type='submit' name='per' value='Tampil' class='submit'>
							</td>
						</tr>
						</form>
					</table>
					
					<p></p>
					
					<table width='98%'>
						<form name='konf_udah' method='post' action='?show=belum'>
						<tr><th colspan='4'>Laporan Konfirmasi Belum Dibaca</th></tr>
						<tr>
							<td align='right' width='80px'>Keseluruhan : </td><td colspan='2'></td>
							<td align='center'><input type='submit' name='all' value='Tampil' class='submit'></td>
						</tr>
						<tr>
							<td align='right' width='80px'>Per Periode :</td>
							<td width='190px'>
								<select name='tgl1' class="tgl"><?php echo combo_tgl_lap(); ?></select>
								<select name='bln1' class="bln"><?php echo combo_bln_lap(); ?></select>
								<select name='thn1' class="thn"><?php echo combo_thn_lap(); ?></select>
							</td>
							<td width='190px'>
								<select name='tgl2' class="tgl"><?php echo combo_tgl_lap(); ?></select>
								<select name='bln2' class="bln"><?php echo combo_bln_lap(); ?></select>
								<select name='thn2' class="thn"><?php echo combo_thn_lap(); ?></select>
							</td>
							<td align='center'>
								<input type='submit' name='per' value='Tampil' class='submit'>
							</td>
						</tr>
						</form>
					</table>

					<p></p>

					<table width='98%'>
						<form name='konf_udah' method='post' action='?show=kirim'>
						<tr><th colspan='4'>Laporan Pengiriman Barang</th></tr>
						<tr>
							<td align='right' width='80px'>Keseluruhan : </td><td colspan='2'></td>
							<td align='center'><input type='submit' name='all' value='Tampil' class='submit'></td>
						</tr>
						<tr>
							<td align='right' width='80px'>Per Periode :</td>
							<td width='190px'>
								<select name='tgl1' class="tgl"><?php echo combo_tgl_lap(); ?></select>
								<select name='bln1' class="bln"><?php echo combo_bln_lap(); ?></select>
								<select name='thn1' class="thn"><?php echo combo_thn_lap(); ?></select>
							</td>
							<td width='190px'>
								<select name='tgl2' class="tgl"><?php echo combo_tgl_lap(); ?></select>
								<select name='bln2' class="bln"><?php echo combo_bln_lap(); ?></select>
								<select name='thn2' class="thn"><?php echo combo_thn_lap(); ?></select>
							</td>
							<td align='center'>
								<input type='submit' name='per' value='Tampil' class='submit'>
							</td>
						</tr>
						</form>
					</table>
			
				<div class="left_bg">
					<a href="" class="close" onclick="window.close()">Tutup</a>
				</div>

			<?php 
				
				} 
			
			}

			?>

		</div>
	</div>

</body>
</html>