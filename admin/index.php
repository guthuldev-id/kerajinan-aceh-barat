<?php

	session_start();
	define ('RONAL', TRUE);

	require_once '../setting.php';
	require_once '../'.fungsi;

	if (!cek_sessi_admin()) {
		require_once login;
	} else {
		if (isset($_GET['act'])) {
			switch($_GET['act']) {
				case 'Home'				: include_once page.'home.php'; break;
				case 'Halaman'			: include_once page.'halaman.php'; break;
				case 'Admin.Lihat'		: include_once page.'admin_view.php'; break;
				case 'Admin.Tambah'		: include_once page.'admin_add.php'; break;
				case 'Admin.Edit'		: include_once page.'admin_edit.php'; break;
				case 'User.Lihat'		: include_once page.'user_view.php'; break;
				case 'User.Detail'		: include_once page.'user_detail.php'; break;
				case 'User.Tambah'		: include_once page.'user_add.php'; break;
				case 'User.Edit'		: include_once page.'user_edit.php'; break;
				case 'Produk.Lihat'		: include_once page.'produk_view.php'; break;
				case 'Produk.Tambah'	: include_once page.'produk_add.php'; break;
				case 'Produk.Edit'		: include_once page.'produk_edit.php'; break;
				case 'Produk.Detail'	: include_once page.'produk_detail.php'; break;
				case 'Jenis.Lihat'		: include_once page.'jenis_view.php'; break;
				case 'Jenis.Tambah'		: include_once page.'jenis_add.php'; break;
				case 'Jenis.Edit'		: include_once page.'jenis_edit.php'; break;
				case 'Jarak.Lihat'		: include_once page.'jarak_view.php'; break;
				case 'Jarak.Tambah'		: include_once page.'jarak_add.php'; break;
				case 'Jarak.Edit'		: include_once page.'jarak_edit.php'; break;
				case 'Pemesanan'		: include_once page.'pesan.php'; break;
				case 'Pesan.Baru'		: include_once page.'pesan_new.php'; break;
				case 'Pesan.Detail'		: include_once page.'pesan_detail.php'; break;
				case 'Pesan.Konfirmasi' : include_once page.'pesan_confirm.php'; break;
				case 'Pesan.Bayar'		: include_once page.'pesan_pay.php'; break;
				case 'Pesan.Kirim'		: include_once page.'pesan_sent.php'; break;
				case 'Berita.Lihat'		: include_once page.'berita_view.php'; break;
				case 'Berita.Detail'	: include_once page.'berita_detail.php'; break;
				case 'Berita.Tambah'	: include_once page.'berita_add.php'; break;
				case 'Berita.Edit'		: include_once page.'berita_edit.php'; break;
				case 'Faq.Lihat'		: include_once page.'faq_view.php'; break;
				case 'Faq.Edit'			: include_once page.'faq_edit.php'; break;
				case 'Faq.Tambah'		: include_once page.'faq_add.php'; break;
				case 'Kontak.Lihat'		: include_once page.'kontak_view.php'; break;
				case 'Kontak.Komen'		: include_once page.'kontak_accept.php'; break;
				case 'Chat.Lihat'		: include_once page.'chat_view.php'; break;
				default	: include_once page.'home.php';
			}
		} else {
			include_once page.'home.php';
		}	
	}

?>