function input_error(tmp) {
	tmp.style.border="1px solid #4a694a";
	tmp.focus();
}

function cek_add_admin() {
	if(document.form_add_admin.user.value==""){
		input_error(document.form_add_admin.user);
		return false;
	}
	if(document.form_add_admin.pass1.value==""){
		input_error(document.form_add_admin.pass1);
		return false;
	}
	if(document.form_add_admin.pass2.value==""){
		input_error(document.form_add_admin.pass2);
		return false;
	}
	return true;
}

function cek_edit_admin() {
	if(document.form_edit_admin.user.value==""){
		input_error(document.form_edit_admin.user);
		return false;
	}
	if(document.form_edit_admin.pass.value==""){
		input_error(document.form_edit_admin.pass);
		return false;
	}
	if(document.form_edit_admin.pass1.value==""){
		input_error(document.form_edit_admin.pass1);
		return false;
	}
	if(document.form_edit_admin.pass2.value==""){
		input_error(document.form_edit_admin.pass2);
		return false;
	}
	return true;
}

function cek_add_user() {
	if(document.form_add_user.nama.value==""){
		input_error(document.form_add_user.nama);
		return false;
	}
	if(document.form_add_user.almt.value==""){
		input_error(document.form_add_user.almt);
		return false;
	}
	if(document.form_add_user.kota.value==""){
		input_error(document.form_add_user.kota);
		return false;
	}
	if(document.form_add_user.pos.value==""){
		input_error(document.form_add_user.pos);
		return false;
	}
	if(document.form_add_user.tlp.value==""){
		input_error(document.form_add_user.tlp);
		return false;
	}
	if(document.form_add_user.mail.value==""){
		input_error(document.form_add_user.mail);
		return false;
	}
	if(document.form_add_user.user.value==""){
		input_error(document.form_add_user.user);
		return false;
	}
	if(document.form_add_user.pass1.value==""){
		input_error(document.form_add_user.pass1);
		return false;
	}
	if(document.form_add_user.pass2.value==""){
		input_error(document.form_add_user.pass2);
		return false;
	}
	return true;
}

function cek_edit_user() {
	if(document.form_edit_user.nama.value==""){
		input_error(document.form_edit_user.nama);
		return false;
	}
	if(document.form_edit_user.almt.value==""){
		input_error(document.form_edit_user.almt);
		return false;
	}
	if(document.form_edit_user.kota.value==""){
		input_error(document.form_edit_user.kota);
		return false;
	}
	if(document.form_edit_user.pos.value==""){
		input_error(document.form_edit_user.pos);
		return false;
	}
	if(document.form_edit_user.tlp.value==""){
		input_error(document.form_edit_user.tlp);
		return false;
	}
	if(document.form_edit_user.mail.value==""){
		input_error(document.form_edit_user.mail);
		return false;
	}
	if(document.form_edit_user.user.value==""){
		input_error(document.form_edit_user.user);
		return false;
	}
	return true;
}

function cek_add_jarak() {
	if(document.form_add_jarak.jarak.value==""){
		input_error(document.form_add_jarak.jarak);
		return false;
	}
	if(document.form_add_jarak.ongkos.value==""){
		input_error(document.form_add_jarak.ongkos);
		return false;
	}
	return true;
}

function cek_edit_jarak() {
	if(document.form_edit_jarak.jarak.value==""){
		input_error(document.form_edit_jarak.jarak);
		return false;
	}
	if(document.form_edit_jarak.ongkos.value==""){
		input_error(document.form_edit_jarak.ongkos);
		return false;
	}
	return true;
}

function cek_add_faq() {
	if(document.form_add_faq.tanya.value==""){
		input_error(document.form_add_faq.tanya);
		return false;
	}
	if(document.form_add_faq.jawab.value==""){
		input_error(document.form_add_faq.jawab);
		return false;
	}
	return true;
}

function cek_edit_faq() {
	if(document.form_edit_faq.tanya.value==""){
		input_error(document.form_edit_faq.tanya);
		return false;
	}
	if(document.form_edit_faq.jawab.value==""){
		input_error(document.form_edit_faq.jawab);
		return false;
	}
	return true;
}

function cek_add_jenis() {
	if(document.form_add_jenis.jenis.value==""){
		input_error(document.form_add_jenis.jenis);
		return false;
	}
	return true;
}

function cek_edit_jenis() {
	if(document.form_edit_jenis.jenis.value==""){
		input_error(document.form_edit_jenis.jenis);
		return false;
	}
	return true;
}

function cek_add_produk() {
	if(document.add_produk.produk.value==""){
		input_error(document.add_produk.produk);
		return false;
	}
	if(document.add_produk.harga.value==""){
		input_error(document.add_produk.harga);
		return false;
	}
	if(document.add_produk.keterangan.value==""){
		input_error(document.add_produk.keterangan);
		return false;
	}
	return true;
}

function cek_edit_produk() {
	if(document.edit_produk.produk.value==""){
		input_error(document.edit_produk.produk);
		return false;
	}
	if(document.edit_produk.harga.value==""){
		input_error(document.edit_produk.harga);
		return false;
	}
	if(document.edit_produk.keterangan.value==""){
		input_error(document.edit_produk.keterangan);
		return false;
	}
	return true;
}

function jumpMenu(targ,selObj,restore) { 
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) 
     selObj.selectedIndex=0;
}